<%@page contentType="text/html" pageEncoding="UTF-8" session="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>
  <head>
    <%@include file="/WEB-INF/jspf/head.jspf"%>
    <title>Home</title>
  </head>
  <body>
    <div class="container-fluid">
      <%@include file="/WEB-INF/jspf/header.jspf"%>
      <div class="row">
        <h1>Welcome to Battle Arena!</h1>
      </div>
      <div class="row">
        <img src="<c:url value="/resources/images/landingpage.jpg"/>" class="img-responsive" alt="Battle Arena">
      </div>  
      <div class="row topBottomMargin">
        <a class="btn btn-info indexButton" href="registration.htm">Register</a>           
      </div>
      <%@include file="/WEB-INF/jspf/footer.jspf"%>
    </div>
  </body>
</html>
