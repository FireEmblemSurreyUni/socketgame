<%-- 
    Document   : userPage
    Created on : Nov 26, 2014, 10:00:40 PM
    Author     : Umesh G
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title>User Page</title>
    </head>
    <body>
        <div class="container-fluid">
          <%@include file="/WEB-INF/jspf/header.jspf"%>
            <div class="row">
                <div class="col-md-12 content">
                    <h1>${pageContext.request.userPrincipal.name}</h1>
                </div>
            </div>
          <%@include file="/WEB-INF/jspf/footer.jspf"%>
        </div>
    </body>
</html>
