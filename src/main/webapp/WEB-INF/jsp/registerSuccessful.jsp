<%-- 
    Document   : registerSuccessful
    Created on : Nov 26, 2014, 10:00:40 PM
    Author     : Umesh G
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title>Registration Successful</title>
    </head>
    <body>
        <div class="container-fluid">
          <%@include file="/WEB-INF/jspf/header.jspf"%>
            <div class="row"><h1>Registration Successful!</h1></div>
          <%@include file="/WEB-INF/jspf/footer.jspf"%>
        </div>
    </body>
</html>
