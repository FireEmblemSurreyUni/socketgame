<%-- 
    Document   : gametest.jsp
    Created on : 13-Nov-2014, 15:36:08
    Author     : rp00091
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" session="false"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="resources/pixi.dev.js"></script>
        <script src="resources/Game.js"></script>
        <script src="resources/GraphicsMgr.js"></script>
        <script src="resources/Map.js"></script>
        <script src="resources/Camera.js"></script>
        <script src="resources/Unit.js"></script>
        <script src="resources/Cursor.js"></script>
        <script src="resources/AnimationController.js"></script>
        <script src="resources/UnitSelection.js"></script>
        <script src="resources/Pathfinder.js"></script>
        <style>
            body { margin: 0 100; }
            #game canvas, #game canvas:focus, #game canvas:hover, #game canvas:selected { outline:none; border:none; cursor:default; }
            #game { width:100%; cursor:default; }
        </style>
    </head>
    <body onload="start()">
        <!-- Added a destroy button for 2 reasons. 1 - because my graphics
             card has no fan, and 2 - to test the removal of the game instance -->
        <script>
            var game;
            function start() {
                game = new Game(document.getElementById('game'), respond);
                
                // TESTING game info message
                
                testMap = '44444444444444444444' + 
                          '40100000000000100004' + 
                          '40000200001000000004' + 
                          '40003444400004001004' + 
                          '40000400000004005004' + 
                          '40020400665104005004' + 
                          '40000000002004000004' + 
                          '40010010004444001004' + 
                          '40000000100000000004' + 
                          '44444444444444444444';
                
                game.receiveMessage({
                    type:21, teamNumber:0, boardWidth:20, board:testMap, 
                    units: [
                        {unitId:0,teamNumber:0,unitType:'soldier',boardPos:106},
                        {unitId:1,teamNumber:1,unitType:'soldier',boardPos:50},
                        {unitId:2,teamNumber:0,unitType:'soldier',boardPos:84},
                        {unitId:3,teamNumber:1,unitType:'soldier',boardPos:107}
                    ]
                });
            }
            
            function terminate() {
                game.destroy();
            }
            
            function respond(msg) {
                if (msg.type === 41) {
                    msg.type = 42;
                    game.receiveMessage(msg);
                } else if (msg.type === 43) {
                    msg.type = 44;
                    msg.hurtUnits = [];
                    game.receiveMessage(msg);
                } else if (msg.type === 45) {
                    msg.type = 46;
                    msg.teamNumber = 1;
                    game.receiveMessage(msg);
                    msg.teamNumber = 0;
                    game.receiveMessage(msg);
                }
            }
        </script>
        <div id="game"></div>
        <button onclick="terminate()">Destroy</button>
    </body>
</html>
