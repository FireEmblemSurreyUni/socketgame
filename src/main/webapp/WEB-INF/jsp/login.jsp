<%-- 
    Document   : register
    Created on : Nov 26, 2014, 10:00:40 PM
    Author     : Umesh G
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title>Login</title>
    </head>
    <body>
        <div class="container-fluid">
          <%@include file="/WEB-INF/jspf/header.jspf"%>
            <div class="row">
                <div class="col-md-offset-5 col-md-3 well">
                    <form class="form-horizontal" name="loginForm" id="loginForm" action="j_spring_security_check" method='POST' role="form">
                        <h4>Sign In</h4>
                        <label class="control-label" form="loginForm" for="username">Username</label>
                        <input class="form-control" type='text' id="username" name='username' value='' placeholder="Enter your username" required>
                        <label class="control-label" form="loginForm" for="password">Password</label>
                        <input class="form-control" type='password' id="password" name='password' value='' placeholder="Enter your password" required>
                        <br>
                        <div id="buttons">  
                            <button class="btn btn-success" type="submit" name="submit">Sign In</button>
                            <a class="btn btn-info" href="registration">Register</a>
                        </div>
                        <br>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
                    </form>      
                </div>
            </div>
          <%@include file="/WEB-INF/jspf/footer.jspf"%>
        </div>
    </body>
</html>
