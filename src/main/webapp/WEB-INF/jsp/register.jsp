<%-- 
    Document   : register
    Created on : Nov 26, 2014, 10:00:40 PM
    Author     : Umesh G
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title>Register</title>
    </head>
    <body>
        <div class="container-fluid">
          <%@include file="/WEB-INF/jspf/header.jspf"%>
            <div class="row">
                <div class="col-md-offset-5 col-md-3 well">
                    <form class="form-horizontal" name="registerForm" id="registerForm" action="/socketgame/registration" method='POST' role="form">
                        <h4>Register</h4>
                        <label class="control-label" form="registerForm" for="username">Username</label>
                        <input class="form-control" type='text' id="username" name='username' value='' placeholder="Enter your username" required>
                        <label class="control-label" form="registerForm" for="email">Email</label>
                        <input class="form-control" type="email" id="email" name='email' value='' placeholder="Enter your email" required>
                        <label class="control-label" form="registerForm" for="password">Password</label>
                        <input class="form-control" type='password' id="password" name='password' value='' placeholder="Enter your password" required>
                        <label class="control-label" form="registerForm" for="confirmPassword">Confirm Password</label>
                        <input class="form-control" type='password' id="confirmPassword" name='confirmPassword' value='' placeholder="Confirm your password" required>
                        <br>
                        <button class="btn btn-success btn-block" type="submit" name="submit">Register</button>
                        <br>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
                    </form>    
                </div>
            </div>
          <%@include file="/WEB-INF/jspf/footer.jspf"%>
        </div>
    </body>
</html>
