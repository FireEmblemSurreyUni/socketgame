<%-- 
    Document   : game
    Created on : Nov 26, 2014, 7:17:23 PM
    Author     : stephenlarkin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <title>Game Page</title>
    </head>
    <body>
        <div class="container-fluid">
          <%@include file="/WEB-INF/jspf/header.jspf"%>
            <div id="bodyContainer">
                <p>Click a name from the users currently connected then click "Challenge User" to challenge them to a game.</p>
                <section id="game"></section>
                <section id="messagerContainer">
                    <div id="messageUI">
                        <div id="messager"></div>
                        <div id="userList"></div>
                    </div>
                    <form id="messageForm">
                        <input type="text" tabindex="0" form="messageSend" id="messageText" name="message"/>
                        <button form="messageSend" type="submit" onclick="lobby.sendMessage()">Send</button>
                    </form>
                </section>
            </div>
          <%@include file="/WEB-INF/jspf/footer.jspf"%>
        </div>
        <script src="resources/SocketHandler.js"></script>
        <script src="resources/Lobby.js"></script>
        <script src="resources/network.js"></script>
        <script src="resources/pixi.dev.js"></script>
        <script src="resources/Game.js"></script>
        <script src="resources/GraphicsMgr.js"></script>
        <script src="resources/Map.js"></script>
        <script src="resources/Camera.js"></script>
        <script src="resources/Unit.js"></script>
        <script src="resources/Cursor.js"></script>
        <script src="resources/AnimationController.js"></script>
        <script src="resources/UnitSelection.js"></script>
        <script src="resources/Pathfinder.js"></script>
    </body>
</html>
