"use strict";

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Ryan Pridgeon
 */

/**
 * The Map class handles the loading and usage of the game map
 * and map grid.
 * 
 * @param {String} tileString - the string containing the tile map ie "11102..
 * @param {int} columns - The number of columns in the map tile grid
 * @param {GraphicsMgr} graphicsMgr - Reference to the GraphicsMgr
 * @returns {Map}
 */
function Map(tileString, columns, graphicsMgr) {
    // Keeps the Map object in scope for functions
    var scope = this;
    
    /** The size of tiles in pixels */
    this.tileSize = 64;
    /** Number of columns in the map grid */
    this.columns = columns;
    /** Number of rows in the map grid */
    this.rows = tileString.length / columns;
    /** The string of tile ID's. For example "110"
     *contains 2 tiles with ID 1 and a tile with ID 0*/
    this.tiles = tileString;
    
    // The list of graphics objects that the map creates
    var graphics = [];
    // Reference to the GraphicsMgr
    var gfxMgr = graphicsMgr;
    
    /////////// LOAD MAP //////////
    
    // For each tile in the tile string
    for (var i = 0; i < this.tiles.length; i++) {
        // Make a graphic for it
        makeTileGraphic(this.tiles[i], i % this.columns, Math.floor(i / this.columns));
    }
    
    // This function takes a tileID (from a tileString) and a column and row
    // and makes it as a graphic.
    function makeTileGraphic(tileID, column, row) {
        // Make a movieclip using the tiles spritesheet
        var mc = gfxMgr.newMC('tiles', GraphicsLayer.BACKGROUND);
        
        // Set mc properties
        mc.anchor.x = 0; mc.anchor.y = 0;
        mc.x = column * scope.tileSize;
        mc.y = row * scope.tileSize;
        
        // Set the frame number to the tileID
        mc.gotoAndStop(parseInt(tileID));
        // Add it to the list of map graphics
        graphics.push(mc);
    }
    
    /**
     * Takes a integer board array position and converts it to world unit x,y coordinates
     * 
     * @param {int} boardPos
     * @returns {{x,y}} unitPos
     */
    this.boardToUnitPos = function(boardPos) {
        var xPos = (boardPos % this.columns) * this.tileSize + this.tileSize / 2;
        var yPos = Math.floor(boardPos / this.columns) * this.tileSize + this.tileSize / 2;
        return { x:xPos, y:yPos };
    }
    
    /**
     * Takes a x,y unit coordinate and returns its position on the 1d board array
     * 
     * @param {{x,y}} unitPos
     * @returns {int} boardPos
     */
    this.unitToBoardPos = function(unitPos) {
        return Math.floor(unitPos.x / this.tileSize) + Math.floor(unitPos.y / this.tileSize) * this.columns;
    }
    
}
