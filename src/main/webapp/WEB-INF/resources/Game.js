"use strict";

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Ryan Pridgeon
 */

var GameState = {
    LOADING: 0, // Loading game assets or waiting for game info
    MY_TURN: 1, // My turn, waiting for player to maek a move
    OPPONENT_TURN: 2, // Opponent's turn, accepting opponents moves from server
    WAITING_FOR_SERVER: 3, // Waiting for server to 'OK' my previous move
};

/**
 * The Game class manages the ingame front end, including graphics
 * and user input. To use it, create a new instance and pass the DOM
 * object to append it to, and a listener object. The listener
 * object must contain a method send() which is called by Game
 * when the user makes a move and an action msg is sent. Pass messages
 * received from the server to receiveMessage().
 * 
 * @param {dom_object} DOM - the DOM object to add the game stage to
 * @param {Function(message)} msgCallback - A callback that is called
 *         when Game wishes to send a message to the server. The parameter
 *         is a message object.
 * @returns {Game} Instance of game
 */
function Game(DOM, msgCallback) {
    this.STAGE_WIDTH = 1024;
    this.STAGE_HEIGHT = 600;
    
    this.send = msgCallback;

    var self = this;
    
    /** The user's team number, set in loadGameInfo */
    this.userTeam = -1;
    
    /** The state variables */
    this.mainState = GameState.LOADING;
    this.graphicsLoaded = false;
    this.gameInfo = null;
    
    /** The queue of game move messages from the server to be processed */
    this.messageQueue = [];
    
    /** The list of game units */
    this.units = [];
    
    /** The PIXI stage, the root of all display objects */
    this.stage = new PIXI.Stage(0x000000);
    this.renderer = PIXI.autoDetectRenderer(this.STAGE_WIDTH, this.STAGE_HEIGHT);
    
    /** The GraphicsMgr instance, pass the onLoaded function to catch when all graphics are loaded */
    this.graphicsMgr = new GraphicsMgr(this.stage, onLoaded );
    
    /** The camera, this is initialised in startGame */
    this.camera = null;
    
    /** The map, this is initialised in startGame */
    this.map = null;
    
    /** The cursor, this is intialised in startgame */
    this.cursor = null;
    
    /** Pathfinder, initialised in startGame */
    this.pathfinder = null;
    
    /** The animation controller */
    this.animator = new AnimationController(this);
    
    /** The selection display */
    this.selector = new UnitSelection(this);
 
    // add the renderer view element to the DOM
    DOM.appendChild(this.renderer.view);
    
    /////////// Add an end turn button
    var endButton = document.createElement("BUTTON");
    endButton.onclick = endTurn;
    endButton.appendChild(document.createTextNode("END TURN"));
    DOM.appendChild(endButton);
    
    /////////// Make simple loading screen
    this.loadingScreen = new PIXI.DisplayObjectContainer();
    var loadingScreenText = new PIXI.Text("Loading..",{fill:"white"});
    loadingScreenText.anchor.x = 0.5; loadingScreenText.anchor.y = 0.5;
    loadingScreenText.x = this.STAGE_WIDTH / 2; loadingScreenText.y = this.STAGE_HEIGHT / 1.5;
    this.loadingScreen.addChild(loadingScreenText);
    this.graphicsMgr.addGraphic(this.loadingScreen, GraphicsLayer.GUI);
    
    /** Text showing whose turn it is */
    this.turnText = null;
    
    // Start game loop
    requestAnimFrame( onEnterFrame );
    
    /**
     * This is called when GraphicsMgr has finished loading assets.
     */
    function onLoaded() {
        self.graphicsLoaded = true;
        self.checkLoaded(); // Check if assets and game info are loaded
    }
    
    var sizeFactor = 0.9;
    function updateSize() {
        var height = Math.floor((window.innerHeight - 20) * sizeFactor);
        self.STAGE_WIDTH = Math.floor(DOM.offsetWidth * sizeFactor);
        var relHeight = Math.floor(self.STAGE_WIDTH * 0.6);
        self.STAGE_HEIGHT = relHeight > height ? height : relHeight;
        self.renderer.resize(self.STAGE_WIDTH, self.STAGE_HEIGHT);
    }
    
    window.onresize = updateSize;
    updateSize();
    
    
    
    
    // If mobile device..
    // http://stackoverflow.com/questions/3514784/what-is-the-best-way-to-detect-a-handheld-device-in-jquery
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        // add touch events
        this.stage.tap = mouseClick;
        this.stage.touchstart = touchStart;
        this.stage.touchend = touchEnd;
        this.stage.touchmove = touchMove;
    } else {
        // Allows keyboard focus
        this.renderer.view.contentEditable = true;
        this.renderer.view.focus();
        
        // Add mouse click event
        this.stage.mousedown = mouseClick;
        // Add key events
        this.renderer.view.addEventListener('keydown', keyDown);
        this.renderer.view.addEventListener('keyup', keyUp);
    }
    
    this.keyStates = [];
    for (var j = 0; j < 256; j++)
        this.keyStates.push(false);
    
    /**
     * Called when a key is pressed
     * 
     * @param {key event} event
     * @returns {undefined}
     */
    function keyDown(event) {
        self.keyStates[event.keyCode] = true;
        
        // Enter key press
        if (event.keyCode === 13) {
            endTurn();
        }
    }
    
    /**
     * Called when a key is released
     * 
     * @param {key event} event
     * @returns {undefined}
     */
    function keyUp(event) {
        self.keyStates[event.keyCode] = false;
    }
    
    /**
     * Called every time the mouse clicks
     * 
     * @param {mouse event} mouseData
     * @returns {undefined}
     */
    function mouseClick(mouseData) {
        
        self.renderer.view.focus();
        
        // If an animation is playing
        if (self.animator.isAnimating())
            self.animator.skip(); // skip it
        // If it's not our turn, and there's an animation playing
        else if (self.mainState !== GameState.MY_TURN)
            return; // ignore this mouse click
        
        // Update cursor
        var mousePos;
        if (mouseData.global.x > 0 && mouseData.global.y > 0)
            mousePos = mouseData.global;
        else
            mousePos = self.stage.getMousePosition();
        
        self.cursor.update(mousePos.x, mousePos.y);
        
        // See if there's a unit at the cursor
        var clickedUnit = null;
        for (var i = 0; i < self.units.length; i++) {
            if (self.units[i].boardPos === self.cursor.getBoardPos() && self.units[i].hp > 0) {
                // units[i] is selected
                clickedUnit = self.units[i];                
            }
        }
        
        // If a unit was clicked
        if (clickedUnit) {
            // If unit is on user's team
            if (clickedUnit.team === self.userTeam) {
                // Select them
                self.selector.selectUnit(clickedUnit);
                
            // If not on team
            } else {
                // If no unit is selected
                if (self.selector.unit === null) {
                    // nothing
                    
                // If clicked unit is within attacking range of selected unit, and unit has energy
                } else if (self.selector.unit.isNeighbour(clickedUnit) && self.selector.unit.energy > 0) {
                    // Attack them
                    
                    // Create attack message
                    /*
                    {
                    type:43,
                        actionId:int,
                        unitId:int,
                        boardPos:int
                    }
                     */
                    var msg = {'type':43, 'actionId':0,
                        'unitId':self.selector.unit.id, 'boardPos':self.cursor.getBoardPos() };
                    self.send(msg);
                    
                    // wait for server response
                    self.mainState = GameState.WAITING_FOR_SERVER;
                } else {
                    self.selector.deselect();
                }
            }
        } else { // Clicking a tile without a unit on it
            
            // If no unit selected
            if (self.selector.unit === null) {
                // Nothing
                
            // If tile is within movement range of selected unit
            } else if (self.pathfinder.findPath(self.selector.unit.boardPos,
                    self.cursor.getBoardPos(), self.selector.unit.energy)) {
                // Move to tile
                
                // Create move message
                /*
                {
                    type:41,
                    actionId:int,
                    unitId:int,
                    boardPos:int
                }
                 */
                var msg = {'type':41, 'actionId':0,
                    'unitId':self.selector.unit.id, 'boardPos':self.cursor.getBoardPos() };
                
                self.send(msg);
                
                // wait for server response
                self.mainState = GameState.WAITING_FOR_SERVER;
            } else {
                // If out of range, deselect unit
                self.selector.deselect();
            }
        }
    }
    
    /**
     * This function is called to draw a new frame every time.
     * It's essentially the draw loop.
     * 
     * @returns {undefined}
     */
    var running = true;
    function onEnterFrame() {
        
        if (running)
            requestAnimFrame( onEnterFrame );
        
        if (self.mainState === GameState.LOADING) {
            
        } else {
           
            // Update cursor
            if( !(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ) {
                var mousePos = self.stage.getMousePosition();
                self.cursor.update(mousePos.x, mousePos.y);
            }
            
            // Show waiting icon if cursor cannot be used
            if (self.mainState === GameState.MY_TURN && !self.animator.isAnimating()) {
                self.cursor.setActive();
            } else {
                self.cursor.setWaiting();
            }
            
            // If an animation is playing
            if (self.animator.isAnimating()) {
                // play anim
                self.animator.update();
            } else {
                // No animation is playing.
                
                // Process messages
                if (self.messageQueue.length > 0) {
                    // Get next message from queue
                    var msg = self.messageQueue.shift();
                    
                    processMessage(msg);
                }
            }
            
            // Let arrow keys and WASD move the camera
            if (self.keyStates[37] || self.keyStates[65]) self.camera.panLeft();
            else if (self.keyStates[39] || self.keyStates[68]) self.camera.panRight();
            if (self.keyStates[38] || self.keyStates[87]) self.camera.panUp();
            else if (self.keyStates[40] || self.keyStates[83]) self.camera.panDown();
            
            self.camera.update();
        }
 
        // render the stage  
        self.renderer.render(self.stage);
        
    }
    
    /**
     * Call this to request ending the turn. Sends an end turn message to the server.
     * @returns {undefined}
     */
    function endTurn() {
        // If it's my turn and there's no animation
        if (self.mainState === GameState.MY_TURN && !self.animator.isAnimating()) {
            // Send an end turn message
            /*client sends end of turn; sent by client
            {
                type:45
            }*/
            var msg = {'type':45 };
            self.send(msg);
            
            // wait for server response
            self.mainState = GameState.WAITING_FOR_SERVER;
            self.selector.deselect();
        }
    }
    this.endTurn = endTurn;
    
    /**
     * Processes a message about an action that was received by the server.
     * 
     * @param {message} msg
     * @returns {undefined}
     */    
    function processMessage(msg) {
        switch (msg.type) {
            case 40:
                /*server rejects an invalid action; sent by server
                {
                        type:40,
                        actionId:int
                }*/
                
                // if waiting for my turn response, return to my turn
                if (self.mainState === GameState.WAITING_FOR_SERVER) {
                    self.mainState = GameState.MY_TURN;
                }
                break;
                
            case 42:
                /*
                 client sends move action to server; sent by client
                    {
                        type:41,
                        actionId:int,
                        unitId:int,
                        boardPos:int
                    }
                 */
                
                self.camera.moveTo(self.map.boardToUnitPos(msg.boardPos));
                self.animator.animateMoveTo(self.units[msg.unitId], msg.boardPos);
                self.selector.deselect();
                
                if (self.mainState === GameState.WAITING_FOR_SERVER) self.mainState = GameState.MY_TURN;
                break;
                
            case 44:
                /*server sends attack action to clients; sent by server
                {
                    type:44,
                    actionId:int,
                    unitId:int,
                boardPos:int,
                    hurtUnits: [ { unitId:int, hp:int } ]
                }*/
                self.camera.moveTo(self.map.boardToUnitPos(msg.boardPos));
                self.animator.animateAttack(self.units[msg.unitId], msg.boardPos);
                self.units[msg.unitId].attack();
                self.selector.deselect();
                
                // Update hurt units
                for (var i in msg.hurtUnits) {
                    self.units[msg.hurtUnits[i].unitId].hurt(msg.hurtUnits[i].hp);
                }
                
                if (self.mainState === GameState.WAITING_FOR_SERVER) self.mainState = GameState.MY_TURN;
                break;
                
            case 46:
                /*server sends end of turn; sent by server
                {
                    type:46,
                teamNumber:int // team whose turn is starting
                }*/
                // Set turn states
                if (self.userTeam === msg.teamNumber)
                    self.mainState = GameState.MY_TURN;
                else
                    self.mainState = GameState.OPPONENT_TURN;
                
                for (var i in self.units) {
                    // Reset units stats
                    self.units[i].resetTurn();
                    
                    // Find position of current teams unit for camera
                    if (self.units[i].team === msg.teamNumber && self.units[i].hp > 0)
                        self.camera.moveTo(self.map.boardToUnitPos(self.units[i].boardPos));
                    
                }
                
                self.turnText.setText(self.mainState === GameState.MY_TURN ? 'Your turn (Press enter to end)' : 'Opponent\'s turn');
        }
    }
    
    // Touch events used for dragging the camera on phones / tablets
    var touchPos = null;
    
    function touchStart(touchData) {
        touchPos = touchData.global;
    }
    
    function touchMove(touchData) {
        if (touchPos && touchData.global) {
            self.camera.shift(touchPos.x - touchData.global.x, touchPos.y - touchData.global.y);
        }
        touchPos = touchData.global;
    }
    
    function touchEnd(touchData) {
        touchPos = null;
    }
    
    /**
     * Call this to destroy the Game cleanly and remove it from the given
     * DOM object.
     * 
     * @param {DOM Object} DOM
     */
    this.destroy = function() {
        running = false;
        this.graphicsMgr.destroy();
        this.renderer.view.remove();
        endButton.remove();
    };
    
    
    return this;
}


/**
 * This checks if the game info and graphics are finished loading.
 * If they are, it loads the game info.
 */
Game.prototype.checkLoaded = function() {
    // If not loaded, return
    if (this.gameInfo === null || this.graphicsLoaded === false) {
        return;
    } else {
        // If loaded, load game info and start game
        this.send({'type':22});
        this.loadGameInfo();
        this.startGame();
    }
};

/**
 * Call this when a message is received.
 * 
 * @param {Message} message
 */
Game.prototype.receiveMessage = function(message) {
    switch (message.type) {
        case 21: // Game info
            this.gameInfo = message;
            this.checkLoaded(); // check if assets and info is loaded
            break;

        default: 
            // If it's a message about a move
            if (message.type >= 40 && message.type <= 46) {
                console.log('Action received');
                console.log(message);
                
                // Add it to the message queue which is polled in the draw loop
                this.messageQueue.push(message);                
            } else {
                // if not supported message type, log error
                console.log("Game received irrelevant message");
                console.log(message);
            }
            break;
    }
};

/**
 * This loads the game info from the game info message.
 * 
 * @returns {undefined}
 */
Game.prototype.loadGameInfo = function() {
    // Get my team
    this.userTeam = this.gameInfo.teamNumber;
    
    // Create the map from the board in gameInfo
    this.map = new Map(this.gameInfo.board, this.gameInfo.boardWidth, this.graphicsMgr);
    
    // For each unit in the game info
    for (var key in this.gameInfo.units) {
        // Get the unit info
        var uniti = this.gameInfo.units[key];
        // Create a new unit
        var newUnit = new Unit(uniti, this.map, this.graphicsMgr);
        // Add to the units list
        this.units[uniti.unitId] = newUnit;
        
    }
};

/**
 * This is called to initialise the game after loading.
 * 
 * @returns {undefined}
 */
Game.prototype.startGame = function() {
    
    // Remove loading screen
    this.graphicsMgr.removeGraphic(this.loadingScreen, GraphicsLayer.GUI);
    
    // Team 0 goes first
    if (this.gameInfo.teamNumber === 0)
        this.mainState = GameState.MY_TURN;
    else
        this.mainState = GameState.OPPONENT_TURN;
    
    // Instantiate Camera
    this.camera = new Camera(this);
    this.camera.panRight();
    
    // Cursor
    this.cursor = new Cursor(this);
    
    // Pathfinder
    this.pathfinder = new Pathfinder(this);
    
    // Find position of current teams unit for camera
    for (var i in this.units)
        if (this.units[i].team === 0)
            this.camera.moveTo(this.map.boardToUnitPos(this.units[i].boardPos));
    
    /////////// Make a graphic saying whose turn it is
    this.turnText = new PIXI.Text("",{fill:"white"});
    this.turnText.anchor.x = 0.5; this.turnText.anchor.y = 0.5;
    this.turnText.x = this.STAGE_WIDTH / 2; this.turnText.y = 30;
    this.graphicsMgr.addGraphic(this.turnText, GraphicsLayer.GUI);
    this.turnText.setText(this.mainState === GameState.MY_TURN ? 'Your turn (Press enter to end)' : 'Opponent\'s turn');
};
