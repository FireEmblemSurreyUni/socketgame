/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Controls the cursor graphic ingame.
 * 
 * @param {Game} game
 * @returns {Cursor}
 */
function Cursor(game) {
    
    // Set screen edge width in pixels for panning the camera
    var panEdge = 50;
    
    // Set up graphic
    var mc = game.graphicsMgr.newMC('cursor', GraphicsLayer.FOREGROUND);
    mc.anchor.x = 0.25;
    mc.anchor.y = 0.25;
    
    var animTimer = 0;
    
    /**
     * Updates the cursor. Call this each frame.
     * 
     * @param {type} x - Screen mouse x coordinate
     * @param {type} y - Screen mouse y coordinate
     * @returns {undefined}
     */
    this.update = function(x, y) {
        // Set graphic's position, locked to nearest tile
        mc.x = Math.floor((game.camera.x + x) / game.map.tileSize) * game.map.tileSize;
        mc.y = Math.floor((game.camera.y + y) / game.map.tileSize) * game.map.tileSize;
        
        // If mouse is on screen
        if (x >= 0 && y >= 0) {
            // Pan if it's at the edge of the screen
            if (x < panEdge) {
                game.camera.panLeft();
            } else if (x > game.STAGE_WIDTH - panEdge) {
                game.camera.panRight();
            }
            if (y < panEdge) {
                game.camera.panUp();
            } else if (y > game.STAGE_HEIGHT - panEdge) {
                game.camera.panDown();
            }
        }
        
        // Animate the alpha fade in and out of the cursor
        if (mc.currentFrame == 0) {
            if (animTimer > Math.PI * 2)
                animTimer -= Math.PI * 2;
            
            mc.alpha = Math.sin(animTimer) / 6 + 0.8333;
            animTimer += 0.07;
        } else {
            mc.alpha = 1;
        }
    };
    
    /**
     * Shows the waiting/hourglass cursor
     * @returns {undefined}
     */
    this.setWaiting = function() {
        mc.gotoAndStop(3);
    }
    
    /**
     * Shows the cursor as normal/active
     * @returns {undefined}
     */
    this.setActive = function() {
        mc.gotoAndStop(0);
    }
    
    /**
     * Returns the board position index that the cursor is currently at.
     * 
     * @returns {Number|int}
     */
    this.getBoardPos = function() {
        return game.map.unitToBoardPos(mc);
    }
}
