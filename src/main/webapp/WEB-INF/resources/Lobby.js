/**
 * Creates a new lobby object, which contains the logic for setting up the game lobby
 * 
 * Need to change this to use the same callback method as the game for sending messages via socket...
 * 
 * @param {websocket} socket
 * @returns {Lobby}
 */
var Lobby = function(socket) {

  // Personal user details. Might make this into an object in a later refactor.
  var uid;
  var status;
  var name;
  
  var userList = {};
  var opponentUid;
  
  // Elements for each of the main components
  var userListElement = document.getElementById("userList");
  var messagerElement = document.getElementById("messager");
  
  var currentMessages = 0;
  // Increase for production, currently set at 8 for testing purposes.
  var messageLimit = 45;
  var lastSenderUid;
  var lastMessageElement;
  
  /**
   * Challenge the specified user to a game
   * 
   * @param {int} challengingUid
   */
  function challengeUser(challengingUid) {
    var message = {
        type: 10
      , opponentUid: challengingUid
      };

      userList[challengingUid].node.classList.remove("selectedUser");
      userList[challengingUid].node.classList.add("challengedUser");

      userList[challengingUid].challengeButton.remove();

      socket.send(message);
  }
  
  /**
   * Sets the challenging user id (uid)
   * 
   * @param {int} uidToSet The user that the challenge is being sent to or received from
   */
  function setUserChallenge(uidToSet) {
    if (!userList[uidToSet].challenged) {
      if (uidToSet !== opponentUid) {
        if (typeof opponentUid !== "undefined" && opponentUid !== null) {
          if (typeof userList[opponentUid].challengeButton !== "undefined") {
            userList[opponentUid].challengeButton.remove();
          }
          userList[opponentUid].node.classList.remove("selectedUser");
        }

        userList[uidToSet].node.classList.add("selectedUser");

        var button = document.createElement("button");
        button.appendChild(document.createTextNode("Challenge User"));
        button.classList.add("userChallenge");
        // Add onclick listener function to the generated button.
        button.addEventListener("click", function() {
          challengeUser(uidToSet);
        });

        userList[uidToSet].challengeButton = button;
        userList[uidToSet].node.appendChild(button);

        opponentUid = uidToSet;
      } else if (typeof opponentUid !== "undefined") {
        userList[uidToSet].challengeButton.remove();
        userList[uidToSet].node.classList.remove("selectedUser");
        opponentUid = null;
      }
    }
  }
  
  /**
   * Sends a message when the enter key is pressed.
   * 
   * @param {type} e
   */
  function sendMessageOnEnter(e) {
    if (!e) e = window.event;
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13){
      sendMessage();
    }
  }
  
  /**
   * Sends a message to all of the other online users
   */
  function sendMessage() {
    var message = document.getElementById("messageText").value;
    if (message !== "") {
      var toSend = {
        type: 6
      , message: message
      };
      socket.send(toSend);
      document.getElementById("messageText").value = "";
    }
  }
  
  /**
   * Helper to add a leading zero to double character input.
   * 
   * @param {String} input
   * @returns {String} Input with a leading zero.
   */
  function addLeadingZero(input) {
    return ("0" + input).slice(-2);
  }
  
  /**
   * Removes the challenge from a specified user.
   * 
   * @param {int} uid
   */
  function clearChallenge(uid) {
    userList[uid].challenged = false;
    userList[uid].acceptButton.remove();
    userList[uid].rejectButton.remove();
  }
  
  /**
   * Accept a challenge from the specified user.
   * 
   * @param {int} uid
   */
  function acceptChallenge(uid) {
    var message = {
      type: 12
    , opponentUid: uid
    };
    
    clearChallenge(uid);
    
    socket.send(message);
  }
  
  /**
   * Rejects a challenge from the specified user.
   * 
   * @param {int} uid
   */
  function rejectChallenge(uid) {
    var message = {
      type: 13
    , opponentUid: opponentUid
    };

    clearChallenge(uid);
    
    socket.send(message);
  }
  
  // Adds the keypress listener to the message box
  document.getElementById("messageText").onkeypress = function(e){
    sendMessageOnEnter(e);
  };

  /**
   * Sends a message through the chat client.
   * 
   * TODO: add some kind of the thingy where strings are cleaned so they aren't horrible (I can't English)
   */
  this.sendMessage = function() {
    sendMessage();
  };
  
  /**
   * Recieves a message from the specified user. Processes and displays as needed.
   * Will only display up to a set number of messages.
   * 
   * @param {int} senderUid Uid of the user sending the message
   * @param {String} message Message that was sent by the user
   */
  this.receiveMessage = function(senderUid, message) {
    // Check if current position is close enough to bottom to scroll down.
    var endScroll = messagerElement.scrollHeight - messagerElement.scrollTop <= messagerElement.clientHeight + 6;
    // Check if we're at the message limit
    if (currentMessages === messageLimit) {
      // remove first element in the messageElement
      var bodyElementCount = messagerElement.getElementsByClassName("messageBody").length;
      messagerElement.firstChild.remove();
      currentMessages = (currentMessages - bodyElementCount) + 1;
    } else {
      currentMessages++;
    }
    
    if (lastSenderUid !== senderUid || typeof lastSenderUid === "undefined") {
      console.log("last sender: " + lastSenderUid + " , new sender: " + senderUid);
      // Construct new element for message to go into
      var newMessage = document.createElement("article");
      newMessage.classList.add("message");
      var messangerName;
      // Resolve the messangerName if it's your own name
      if (senderUid === uid) {
        messangerName = name;
      } else {
        messangerName = userList[senderUid].name;
      }
      var userName = document.createTextNode(messangerName + ":");
      var userNameElement = document.createElement("header");
      userNameElement.classList.add("username");
      newMessage.appendChild(userNameElement).appendChild(userName);
      
      lastMessageElement = newMessage;
      lastSenderUid = senderUid;
      
      messagerElement.appendChild(lastMessageElement);
    }
    
    // Add our constructed element to the list
    var messageElement = document.createElement("section");
    messageElement.classList.add("messageBody");
    var time = new Date();
    var timeElement = document.createElement("time");
    timeElement.dateTime = time;
    timeElement.appendChild(document.createTextNode("[" + addLeadingZero(time.getHours()) + ":" + addLeadingZero(time.getMinutes()) + ":" + addLeadingZero(time.getSeconds()) + "]"));
    messageElement.appendChild(timeElement);
    var messageElementText = document.createTextNode(message);
    var textElement = document.createElement("p");
    lastMessageElement.appendChild(messageElement).appendChild(textElement).appendChild(messageElementText);
    
    // If the messanger is at the bottom (aka someone's not scrolling up) then move the scroll to the bottom of the element
    if(messagerElement.scrollHeight > messagerElement.clientHeight && endScroll) {
      messagerElement.scrollTop = messagerElement.scrollHeight - messagerElement.clientHeight;
    }
  };
  
  /**
   * Set the user id of the current user and the current user's name.
   * @param {object} data Initial user information sent by the server.
   */
  this.setUser = function(data) {
    uid = data.uid;
    name = data.name;
  };
  
  /**
   * Remove the specified user from the userlist.
   * @param {int} uid
   */
  this.removeUser = function(uid) {
    // Remove the html list node for the specified user.
    this.unsetChallenge(uid);
    userList[uid].node.remove();
    delete userList[uid];
  };
  
  /**
   * Sets a new status for the specified user
   * @param {int} toSetUid
   * @param {String} newStatus
   */
  this.setUserStatus = function(toSetUid, newStatus) {
    if (toSetUid === uid) {
      status = newStatus;
    } else {
      if (userList[toSetUid].status === "ingame") {
        userList[toSetUid].node.classList.remove("userInGame");
      }
      userList[toSetUid].status = newStatus;
      if (newStatus === "ingame") {
        userList[toSetUid].node.classList.add("userInGame");
      }
    }
  };

  /**
   * Adds a user to the list of current online users
   * @param {object} data
   */
  this.addUser = function(data) {
    userList[data.uid] = {name: data.name, status: data.status, challenged: false, request: false};
    var listNode = document.createElement("div");
    var textNode = document.createTextNode(data.name);
    var userName = document.createElement("div");
    userName.classList.add("user");
    userListElement.appendChild(listNode).appendChild(userName).appendChild(textNode);
    listNode.onclick = function() {
      setUserChallenge(data.uid);
    };
    userList[data.uid].node = listNode;
  };
  
  /**
   * Sets an incoming challenge up from the specified user and displays the message asking the user to accept or reject.
   * @param {int} uid The user who is challenging
   */
  this.setChallenge = function(uid) {
    if (!userList[uid].challenged && userList[uid].status !== "ingame") {
      var infoNode = document.createElement("div");
      infoNode.classList.add("gameInfo");
      infoNode.appendChild(document.createTextNode("You have been challenged by " + userList[uid].name + "."));
      messagerElement.appendChild(infoNode);
      
      userList[uid].node.classList.add("challengedBy");
      
      var acceptNode = document.createElement("button");
      acceptNode.appendChild(document.createTextNode("Accept"));
      acceptNode.addEventListener("click", function() {
         acceptChallenge(uid);
      });
      
      var rejectNode = document.createElement("button");
      rejectNode.appendChild(document.createTextNode("Reject"));
      rejectNode.addEventListener("click", function() {
         rejectChallenge(uid);
      });
      
      userList[uid].node.appendChild(acceptNode);
      userList[uid].node.appendChild(rejectNode);
      
      userList[uid].acceptButton = acceptNode;
      userList[uid].rejectButton = rejectNode;
      userList[uid].challenged = true;
    }
  };
  
  /**
   * Remove the currently specified challenge for whatever reason.
   * @param {int} uid The user who had sent the challenge
   */
  this.unsetChallenge = function(uid) {
    if (userList[uid].challenged) {
      clearChallenge(uid);
    }
  };
  
  /**
   * Do the setup for starting the game.
   */
  this.startGame = function() {
    document.getElementById("messagerContainer").style.display = "none";
  };
  
  /**
   * Reset the messager after the game has been played.
   */
  this.reset = function() {
    messagerElement.remove();
    userListElement.remove();
    userList = {};
    
    messagerElement = document.createElement("div");
    messagerElement.id = "messager";
    userListElement = document.createElement("div");
    userListElement.id = "userList";
    var toAdd = document.getElementById("messageUI");
    toAdd.appendChild(messagerElement);
    toAdd.appendChild(userListElement);
    lastSenderUid = null;
    document.getElementById("messagerContainer").style.display = "block";
    opponentUid = null;
  };
};
