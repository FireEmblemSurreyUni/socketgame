/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Reference;
 * http://en.wikipedia.org/wiki/A*_search_algorithm
 * 
 * The pathfinder class can find a path for a unit to walk between tiles
 * without colliding with walls or other units. Uses a modified A* search algorithm
 * to find the path.
 * 
 * @param {Game} gamePass
 * @returns {Pathfinder}
 */
function Pathfinder(gamePass) {
    
    var SOLID = '4';
    
    var game = gamePass;
    
    var nodes = [];
    var boardWidth = game.map.columns;
    var boardHeight = game.map.rows;
    
    // Build set of pathfinding nodes
    for (var i = 0; i < game.map.tiles.length; i++) {
        nodes.push(new PathfinderNode());
    }
    
    /**
     * Finds a path from the given startPos board position and destPos board position
     * indices. Travellimit is the maximum number of tiles you can walk across to get there.
     * 
     * @param {type} startPos
     * @param {type} destPos
     * @param {type} travelLimit
     * @returns {Array|Pathfinder.findPath.path}
     */
    this.findPath = function(startPos, destPos, travelLimit) {
        var i;
        // Get a collision map from the tiles in the map
        pathMap = game.map.tiles;
        
        // Add units as solid in collision map
        for (i in game.units) {
            if (game.units[i].hp > 0 && game.units[i].boardPos !== startPos)
                pathMap = pathMap.substr(0,game.units[i].boardPos) + '9' + pathMap.substr(game.units[i].boardPos + 1);
        }
        
        // Initiate set of pathfinding nodes
        for (i = 0; i < pathMap.length; i++) {
                 
            nodes[i].closed = false;
            nodes[i].hScore = calculateHScore(i, destPos);
            nodes[i].gScore = -1;
            nodes[i].fScore = -1;
            nodes[i].cameFrom = -1;
            nodes[i].neighbours = [];
            
            if (pathMap.charAt(i) < SOLID) {
                nodes[i].solid = false;
                
                // neighbour to left
                if (getCol(i) > 0 && pathMap.charAt(i-1) < SOLID) {
                    nodes[i].neighbours.push(i-1);
                }
                // neighbour to right
                if (getCol(i) < boardWidth - 1 && pathMap.charAt(i+1) < SOLID) {
                    nodes[i].neighbours.push(i+1);
                }
                // neighbour to top
                if (getRow(i) > 0 && pathMap.charAt(i - boardWidth) < SOLID) {
                    nodes[i].neighbours.push(i-boardWidth);
                }
                // neighbour to bottom
                if (getRow(i) < boardHeight - 1 && pathMap.charAt(i + boardWidth) < SOLID) {
                    nodes[i].neighbours.push(i+boardWidth);
                }
            } else {
                nodes[i].solid = true;
            }
        }
        
        // Initiate open set
        var openSet = [ startPos ];
        nodes[startPos].gScore = 0;
        nodes[startPos].fScore = nodes[startPos].hScore;
        
        // While open set isn't empty
        while (openSet.length > 0) {
            // Find node with lowest f score
            var lowestF = 9999;
            var current = 0;
            for (i = 0; i < openSet.length; i++) {
                if (nodes[openSet[i]].fScore < lowestF) {
                    lowestF = nodes[openSet[i]].fScore;
                    current = openSet[i];
                }
            }
            
            // If it's the goal, finished
            if (current === destPos) {
                // Retrace the route
                var path = [current];
                while (current !== startPos) {
                    current = nodes[current].cameFrom;
                    path.unshift(current);
                }
                return path;
            }
            
            // Remove current from open set
            openSet.splice(openSet.indexOf(current), 1);
            nodes[current].closed = true;
            
            // For each neighbour
            var neighbours = nodes[current].neighbours;
            for (i in neighbours) {
                if (nodes[neighbours[i]].closed)
                    continue;
                
                var tentativeGScore = nodes[current].gScore + 1;
                
                // Modification to the standard A* - if we're over the travel limit, ignore this neighbour
                if (tentativeGScore > travelLimit)
                    continue;
                
                if (openSet.indexOf(neighbours[i]) === -1 || tentativeGScore < nodes[neighbours[i]].gScore) {
                    nodes[neighbours[i]].cameFrom = current;
                    nodes[neighbours[i]].gScore = tentativeGScore;
                    nodes[neighbours[i]].fScore = tentativeGScore + nodes[neighbours[i]].hScore;
                    if (openSet.indexOf(neighbours[i]) === -1)
                        openSet.push(neighbours[i]);
                }
            }
        }
        
        return null;
    }
    
    // Calculates the heuristic score between 2 tiles using the manhatten method (reference https://www.youtube.com/watch?v=C0qCR18gXdU)
    function calculateHScore(startPos, destPos) {
        return Math.abs(Math.floor(startPos / boardWidth) - Math.floor(destPos / boardWidth))
                + Math.abs((startPos % boardWidth) - (destPos % boardWidth));
    }
    
    // Gets the column of the given board position
    function getCol(boardPos) {
        return boardPos % boardWidth;
    }
    
    // Gets the row of the given board position
    function getRow(boardPos) {
        return Math.floor(boardPos / boardWidth);
    }
}

// A node in the pathfinder
function PathfinderNode() {
    this.solid = true;
    this.closed = false;
    this.cameFrom = -1;
    this.gScore = -1;
    this.hScore = -1;
    this.fScore = -1;
    this.neighbours = [];
}