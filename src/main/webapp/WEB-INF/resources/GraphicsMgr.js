"use strict";

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Ryan Pridgeon
 */

/**
 * This is an enum describing the different graphics layers in GraphicsMgr.
 */
var GraphicsLayer = {
    BACKGROUND : 0,
    WORLD : 1,
    FOREGROUND : 2,
    GUI : 3,
    NUM_LAYERS : 4
};

/**
 * The graphics keeps track of all ingame graphics using a list of layers containing
 * display objects. It is used by calling addGraphic and removeGraphic with given
 * layers and display objects.
 * 
 * It also loads all graphics and spritesheets. Pass an onLoaded function to be called
 * when loading is complete. Do not try to use graphics before loading is complete.
 * 
 * @param {PIXI.Stage} pixiStage
 * @param {Function} onLoaded
 * @returns {GraphicsMgr}
 */
function GraphicsMgr(pixiStage, onLoaded) {
    this.stage = pixiStage;
    
    ///////////////////////////// ASSET LOADING //////////////////////////////////////
    
    // Count how many assets are loaded
    var assetsLoaded = 0;
    var assetsToLoad = 0;
    this.assetLoaded = function() {
        assetsLoaded++;
        // If all assets are loaded
        if (assetsLoaded >= assetsToLoad) {
            console.log('Loaded ' + assetsLoaded + ' assets');
            // Call onLoadedFunc
            onLoaded();
        }
    }
    
    // Load resources
    this.spritesheetNames = [ 'scoutblue', 'scoutred', 'soldierblue', 'soldierred', 'tiles', 'cursor' ];
    this.spritesheets = []; 
    
    // For each spritesheet
    for (var i = 0; i < this.spritesheetNames.length; i++) {
        assetsToLoad++;
        // Make a loader
        this.spritesheets[i] = new PIXI.SpriteSheetLoader
                ('resources/sprites/' + this.spritesheetNames[i] + '.json', false);
        this.spritesheets[i].spritesheetName = this.spritesheetNames[i];
        // When it's loaded
        var scope = this;
        this.spritesheets[i].on ('loaded', function() { 
            // Build a list of textures from the frames and assign it to the spritesheet
            this.textures = [];
            for (var frame in this.json.frames) {
                this.textures.push(PIXI.Texture.fromFrame(frame.toString()));
            }
            console.log('loaded ' + this.spritesheetName);
            // Tell the GraphicsMgr that it's loaded
            scope.assetLoaded();
        });
        this.spritesheets[i].load();
    }
    
    ////////////////////////////////////// GRAPHICS MANAGEMENT ////////////////////////
    
    /**
     * This is the list of display object containers, with one for each layer.
     * The graphics in a higher layer will always be drawn infront of graphics
     * on a lower layer. The layers can be accessed using the GraphicsLayer enum.
     */
    this.container = new PIXI.DisplayObjectContainer();
    this.layers = [];
    
    // Initialise the layers
    for (var i = 0; i < GraphicsLayer.GUI; i++) {
        this.layers.push(new PIXI.DisplayObjectContainer());
        this.container.addChild(this.layers[i]);
    }
    this.stage.addChild(this.container);
    
    // Add gui layer seperately to be unaffected from camera
    this.layers.push(new PIXI.DisplayObjectContainer());
    this.stage.addChild(this.layers[GraphicsLayer.GUI]);
    
    return this;
}

/**
 * Call this to destroy the instance of GraphicsMgr cleanly.
 * 
 * @returns none
 */
GraphicsMgr.prototype.destroy = function() {
    // For each layer
    for (var i = 0; i < GraphicsLayer.NUM_LAYERS; i++) {
        // Remove all graphics from layer
        while (this.layers[i].children.length > 0)
            this.layers[i].removeChildAt(0);

    }
    // Remove layer from stage
    this.stage.removeChild(this.container);
    this.stage.removeChild(this.layers[GraphicsLayer.GUI]);
};

/**
 * This function adds a graphic to the screen on the given layer.
 * 
 * @param {PIXI.DisplayObject} graphic 
 * @param {GraphicsLayer enum} layer
 * @returns {PIXI.DisplayObject}
 */
GraphicsMgr.prototype.addGraphic = function(graphic, layer) {
    // If layer is valid
    if (layer < 0 || layer >= GraphicsLayer.NUM_LAYERS)
        return null;

    // Add the graphic to the layer
    this.layers[layer].addChild(graphic);
    return graphic;
};

/*
 * This function makes a new animated movieclip from the given spritesheet
 * name and adds it to the given layer.
 * 
 * @param {String} spritesheet name
 * @param {GraphicsLayer enum} layer
 * @returns {PIXI.MovieClip}
 */
GraphicsMgr.prototype.newMC = function(name, layer) {
    var i;
    var spritesheet = null;

    // Find the right spritesheet by name
    for (var i = 0; i < this.spritesheetNames.length; i++)
        if (this.spritesheetNames[i] === name)
            spritesheet = this.spritesheets[i];

    // If not found, return null
    if (spritesheet === null) return null;

    // Create a MovieClip from the spritesheet
    var mc = new PIXI.MovieClip(spritesheet.textures);

    // Add it to the GraphicsMgr
    this.addGraphic(mc, layer);

    // center the anchor
    mc.anchor.x = 0.5; mc.anchor.y = 0.5;
    return mc;
};

/**
 * This function removes a graphic on the given layer.
 * 
 * @param {PIXI.DisplayObject} graphic 
 * @param {GraphicsLayer enum} layer
 * @returns none
 */
GraphicsMgr.prototype.removeGraphic = function(graphic, layer) {
    // if the layer is valid
    if (layer < 0 || layer >= GraphicsLayer.NUM_LAYERS)
        return null;

    // remove the graphic from the layer
    this.layers[layer].removeChild(graphic);
};

GraphicsMgr.prototype.scale = function(scaleFactor) {
    this.container.scale.x = scaleFactor;
    this.container.scale.y = scaleFactor;
    this.layers[GraphicsLayer.GUI].scale.x = scaleFactor;
    this.container.scale.y = scaleFactor;
};