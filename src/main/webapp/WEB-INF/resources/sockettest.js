console.log("Starting javascript.");

var url = "ws://localhost:8080/socketgame/wstest";

var socket = new WebSocket(url);
var countElement;
var messagesSent = 0;
var json = {
  title: "Thing"
, content: "Text being text saying things about other things."
, id: 10
};
console.log("Thing 2");

// function setUpSockets() {
//   console.log("Set up websockets.");
// }

socket.onopen = function (event) {
  socket.send("Connecting");
  console.log("Connection open.");
};

socket.onerror = function (event) {
  console.log("Error connecting to websockets.");
};

socket.onmessage = function (event) {
  console.log(event.data);
  var listNode = document.createElement("LI");
  var textNode = document.createTextNode(event.data);
  listNode.appendChild(textNode);
  document.getElementById("responses").appendChild(listNode);
};

function updateSentMessages() {
  document.getElementById("count").innerHTML = ++messagesSent;
}

function sendMessage() {
  var messagetext = document.getElementById("messagetext").value;
  //socket.send(JSON.stringify(json));
  socket.send(messagetext);
  updateSentMessages();
}

console.log("Thing 3");

// setUpSockets();