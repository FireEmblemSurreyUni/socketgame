/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Shows a unit as being selected in the game. Shows graphics for the tiles
 * it's on, where it can move to etc.
 * 
 * @param {type} game
 * @returns {UnitSelection}
 */
function UnitSelection(game) {
    
    // The list of graphic objects currently made by the selection
    var graphics = [];
    
    /** Currently selected unit */
    this.unit = null;
    
    /**
     * Selects the given unit
     * 
     * @param {Unit} unit
     * @returns {undefined}
     */
    this.selectUnit = function(unit) {
        
        if (graphics.length > 0)
            this.deselect();
        
        this.unit = unit;
        
        // Show highlighted cursor graphic on the unit's own tile
        var mainMC = game.graphicsMgr.newMC('cursor', GraphicsLayer.BACKGROUND);
        var unitPos = game.map.boardToUnitPos(unit.boardPos);
        mainMC.x = unitPos.x;
        mainMC.y = unitPos.y;
        mainMC.alpha = 0.5;
        graphics.push(mainMC);
        
        // Make a buffer to record status of each tile to loop through
        var processedTiles = [];
        for (var i = 0; i < game.map.tiles.length; i++) {
            processedTiles.push(false);
        }
        
        // For each tile that could possibly be in range
        var unitC = unit.boardPos % game.map.columns;
        var unitR = Math.floor(unit.boardPos / game.map.columns);
        var cMin = Math.max(unitC - unit.energy, 0);
        var cMax = Math.min(unitC + unit.energy, game.map.columns);
        var rMin = Math.max(unitR - unit.energy, 0);
        var rMax = Math.min(unitR + unit.energy, game.map.rows);
        var path;
        for (var c = cMin; c <= cMax; c++) {
            for (var r = rMin; r <= rMax; r++) {
                // If already processed, or the unit's own position, continue
                if (processedTiles[c + r * game.map.columns] || c + r * game.map.columns === unit.boardPos)
                    continue;
                
                // If tile has a possible path to it
                path = game.pathfinder.findPath(unit.boardPos, c + r * game.map.columns, unit.energy);
                if (path !== null) {
                    path.shift();
                    // For each tile in the path
                    while (path.length > 0) {
                        
                        if (!processedTiles[path[0]]) {
                            // Make a graphic
                            var tileMC = game.graphicsMgr.newMC('cursor', GraphicsLayer.BACKGROUND);
                            var tilePos = game.map.boardToUnitPos(path[0]);
                            tileMC.x = tilePos.x;
                            tileMC.y = tilePos.y;
                            tileMC.gotoAndStop(1);
                            graphics.push(tileMC);
                        }
                        
                        processedTiles[path[0]] = true;
                        path.shift();
                    }
                }
            }
        }
        // For each unit
        for (var i in game.units) {
            // If it's a neighbour on the opposite team and isn't dead
            if (unit.isNeighbour(game.units[i]) && game.units[i].team != unit.team && game.units[i].hp > 0) {
                // Make a graphic
                var tileMC = game.graphicsMgr.newMC('cursor', GraphicsLayer.BACKGROUND);
                var tilePos = game.map.boardToUnitPos(game.units[i].boardPos);
                tileMC.x = tilePos.x;
                tileMC.y = tilePos.y;
                tileMC.gotoAndStop(2);
                graphics.push(tileMC);
            }
        }
    }
    
    /**
     * Deselects the selection.
     * 
     * @returns {undefined}
     */
    this.deselect = function() {
        for (var i in graphics) {
            game.graphicsMgr.removeGraphic(graphics[i], GraphicsLayer.BACKGROUND);
        }
        this.unit = null;
    }
}
