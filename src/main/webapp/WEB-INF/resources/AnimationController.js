"use strict";

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Controls animations in the game. Has functions for animating units
 * moving or attacking to different board position.
 * 
 * @param {type} game
 * @returns {AnimationController}
 */
function AnimationController(game) {
    
    // If true, an animation is in progress
    var animating = false;
    
    // If true, animation is an attack animation, if false it's a move animation
    var attackAnimation = false;
    
    // The unit currently being animated
    var unit = null;
    
    // The path for the current unit being moved
    var path = null;
    
    // The destination board position for a unit moving
    var destBoardPos = 0;
    
    var skip = false;
    
    var timer = 0;
    
    /**
     * Updates the animation. Call this each frame.
     * @returns {undefined}
     */
    this.update = function() {
        if (!animating) return;
        
        // Is an attack animation
        if (attackAnimation) {
            
            if (timer > 0 && skip === false) {
                timer--;
            } else {
                animating = false;
                unit.showNormal();
                unit = null;
            }
            
        // Is a movement animation
        } else {
            if (unit.animating) { // If unit is still animating
                if (skip === true) {
                    while (unit.animating)
                        unit.updateAnimation();
                } else {
                    unit.updateAnimation(); // Update animation
                }

            } else {// Unit has finished its animation

                // If the unit is at its destination
                if (unit.boardPos === destBoardPos) {
                    animating = false; // Finish animation
                    unit = null;
                } else {
                    // Unit is not yet at destination

                    // Make move to next tile
                    unit.animateMoveTo(path[0]);
                    unit.updateAnimation();
                    path.shift();
                }
            }
        }
    };
    
    /**
     * Tells the animator to skip through the current animation
     * 
     * @returns {undefined}
     */
    this.skip = function() {
        if (!animating) return;
        else skip = true;
    };
    
    /**
     * Starts animating a unit moving to the given board position.
     * 
     * @param {Unit} unitToMove
     * @param {int} boardPos
     * @returns {undefined}
     */
    this.animateMoveTo = function(unitToMove, boardPos) {
        skip = false;
        unit = unitToMove;
        destBoardPos = boardPos;
        animating = true;
        attackAnimation = false;
        // Find path
        path = game.pathfinder.findPath(unit.boardPos, destBoardPos, unit.energy);
        
        // If can't find a path
        if (path === null) {
            // Just quick-animate it there
            path = [];
            unit.animateMoveTo(boardPos);
        } else {
            path.shift();
        }
    };
    
    /**
     * Starts animating a unit attacking a given board position
     * 
     * @param {Unit} unitToAttack
     * @param {int} boardPos
     * @returns {undefined}
     */
    this.animateAttack = function(unitToAttack, boardPos) {
        skip = false;
        unit = unitToAttack;
        unit.showAttacking(boardPos);
        timer = 30;
        animating = true;
        attackAnimation = true;
    };
    
    /**
     * Returns true if an animation is in progress, false if not
     * 
     * @return {boolean}
     */
    this.isAnimating = function() {
        return animating;
    };
}
