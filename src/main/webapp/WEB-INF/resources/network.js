/*
 *  I am sorry this is so poorly formatted. 
 *  TODO: Split up into multiple files and clean up function/variable declaration.
 */

"use strict";

var windowLoc = window.location;
var url = "ws://" + windowLoc.hostname + ":8080/socketgame/wstest";

var socket = new SocketHandler(url, "lobby", function(event){
  // This should actually be handled by the server/user model
  var onOpenJSON = {
    type: 0
  , name: "TestUser"
  };
    
  return JSON.stringify(onOpenJSON);
});
  
var lobby = new Lobby(socket);
var game;
var gameElement = document.getElementById("game");

// Error handling
socket.onerror = function (event) {
  alert("There has been an error connecting to the server.");
};

// Bind the required functions.
// Set assigned uid and get userlist of available users.
socket.bind("lobby", 1, function(data) {
  lobby.setUser(data);
})

// Add a user to the lobby
.bind("global", 2, function(data) {
  lobby.addUser(data);
})

// Remove a user from the lobby
.bind("global", 3, function(data) {
  lobby.removeUser(data.uid);
})

// Update user status
.bind("global", 4, function(data) {
  lobby.setUserStatus(data.uid, data.status);
})

// Add an arbitrary number of users to the lobby
.bind("global", 5, function(data){
  for (var user in data.userlist) {
    lobby.addUser(data.userlist[user]);
  }
})

// Recieve a message from a user to the lobby
.bind("global", 7, function(data){
  console.log(data);
  console.log(data.uid);
  lobby.receiveMessage(data.uid, data.message);
})

// Receive a ping from teh server
.bind("global", 8, function(data) {
  socket.send({type:8});
})

// User recieves a challenge
.bind("lobby", 11, function(data) {
  lobby.setChallenge(data.opponentUid);
})

// Challenge is cancelled by challenger.    
.bind("lobby", 16, function(data) {
  lobby.unsetChallenge(data.opponentUid);
})

// Start the game
.bind("lobby", 20, function(data) {
  socket.switchMode("game");
  lobby.startGame();
  // Start game
  game = new Game(gameElement, socket.send);
})

// Receive game info
.bind("game", 21, function(data) {
  game.receiveMessage(data);
})

// Recieve ready
.bind("game", 23, function(data) {
  null;
})

// Cancel game
.bind("game", 24, function(data) {
  console.log("Game cancelled");
  game.destroy(gameElement);
  lobby.reset();
})

// Game finished
.bind("game", 25, function(data) {
  console.log("Game finished");
  game.destroy(gameElement);
  lobby.reset();
})

// Invalid action
.bind("game", 40, function(data) {
  game.receiveMessage(data);
})

// Move action OK
.bind("game", 42, function(data) {
  game.receiveMessage(data);
})

// Server attack action OK
.bind("game", 44, function(data) {
  game.receiveMessage(data);
})

// Server sends attack action
.bind("game", 46, function(data) {
  game.receiveMessage(data);
});