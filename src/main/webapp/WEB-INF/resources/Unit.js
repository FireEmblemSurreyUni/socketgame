"use strict";

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Ryan Pridgeon
 */

var Direction = {
    UP:0,
    RIGHT:1,
    DOWN:2,
    LEFT:3
};

/**
 * Unit describes a single game unit such as a soldier on the game board.
 * It's constructed from a unitDescription, which is taken from the
 * gameinfo message sent by the server at the start of the game. (In
 * the 'units' field of the message is an array of unitDescriptions)
 * 
 * @param {unitDescription} unitDescription - an element of the 'units' field
 *          in the game info message
 * @returns {Unit}
 */
var Unit = function(unitDescription, map, gfxMgr) {
    var self = this;
    var map = map;
    
    /** The unit's unique ID */
    this.id = unitDescription.unitId;
    
    /** The team number that this unit is on */
    this.team = unitDescription.teamNumber;
    
    /** The unit's type */
    this.type = unitDescription.unitType;
    
    /** The unit's health points (hp) */
    this.hp = 100;
    
    /** The position of the unit in the map grid/board array */
    this.boardPos = unitDescription.boardPos;
    
    /** The energy of the unit is how many more board positions he can travel before he is too tired for this turn */
    var maxEnergy;
    
    /** How much energy is lost from attacking */
    var attackEnergy;
    
    // Set stats based on unit type
    if (this.type == 'soldier') {
        maxEnergy = 6;
        attackEnergy = maxEnergy;
    } else if (this.type == 'scout') {
        maxEnergy = 10;
        attackEnergy = 5;
    }
    
    /** Remaining energy this turn for movement / attack etc */
    this.energy = maxEnergy;
    
    var direction = Math.random() > 0.5 ? 
            (Math.random() > 0.5 ? Direction.UP : Direction.RIGHT) : (Math.random() > 0.5 ? Direction.DOWN : Direction.LEFT );
    
    /** The mc graphic object for this unit */
    var spritesheetName = this.type + (this.team === 1 ? 'red' : 'blue');
    var mc = gfxMgr.newMC(spritesheetName, GraphicsLayer.WORLD);
    var pos = map.boardToUnitPos(this.boardPos); // Convert board position to screen x,y
    mc.x = pos.x;
    mc.y = pos.y;
    mc.gotoAndStop(direction * 4);
    
    console.log('created unit with sprite ' + spritesheetName);
    
    /** This is set to true while the unit is playing an animation or movement */
    this.animating = false;
    
    /** The target position, direction and velocity for the movement animation */
    var targetPos = { x:pos.x, y:pos.y };
    var velocity = { x:0, y:0 };
    
    /** This counts frames for the animation. It decrements during the animation until 0 */
    var animCounter = 0;
    
    /** The frame sequence for the animation. frame 0 then frame 1 then frame 0 then frame 2 for walking */
    var animSequence = [ 0, 1, 0, 2 ];
    
    /** The amount of frames it takes to move one tile */
    var moveRate = 20;
    
    // Make a HP bar
    var hpBarBackground = new PIXI.Graphics();
    hpBarBackground.beginFill(0x000000,1);
    hpBarBackground.drawRect(-2, -2, 34, 7);
    hpBarBackground.endFill();
    hpBarBackground.x = -15;
    hpBarBackground.y = 25;
    mc.addChild(hpBarBackground);
    var hpBarFill = new PIXI.Graphics();
    hpBarFill.beginFill(this.team === 0 ? 0x5555FF : 0xFF5555,1);
    hpBarFill.drawRect(0, 0, 30, 3);
    hpBarFill.endFill();
    hpBarFill.x = -15;
    hpBarFill.y = 25;
    mc.addChild(hpBarFill);
    updateHpBar();
    
    // Make an energy bar
    
    /**
     * This tells the unit to start animating movement towards the given board position.
     * 
     * @param {int} boardPos - position in the 1d board grid
     * @returns {undefined}
     */
    this.animateMoveTo = function(boardPos) {
        targetPos = map.boardToUnitPos(boardPos);
        
        // Work out which direction for the graphic to face
        if (mc.y > targetPos.y) {
            direction = Direction.UP;
        } else if (mc.x < targetPos.x) {
            direction = Direction.RIGHT;
        } else if (mc.y < targetPos.y) {
            direction = Direction.DOWN;
        } else {
            direction = Direction.LEFT;
        }
        
        // Work out the velocity for the movement
        velocity.x = (targetPos.x - mc.x) / moveRate;
        velocity.y = (targetPos.y - mc.y) / moveRate;
        
        // Start the animation
        animCounter = moveRate;
        this.animating = true;
    };
    
    /**
     * This updates the unit's animation. Call it each frame.
     * 
     * @returns {undefined}
     */
    this.updateAnimation = function() {
        // If being animated
        if (this.animating) {
            if (animCounter > 0) {
                // Work out how far through the animation sequence we are [0-3]
                var animPos = Math.floor(animCounter / moveRate * 4 - 0.05);
                // Show the appropriate frame from the sequence in the correct direction
                mc.gotoAndStop(animSequence[animPos] + direction * 4 );

                // Update graphic position
                mc.x += velocity.x;
                mc.y += velocity.y;
                
                // Update the counter
                animCounter--;
            } else {
                // Set position to target position
                mc.x = targetPos.x;
                mc.y = targetPos.y;
                this.boardPos = map.unitToBoardPos(mc);
                
                // Finish animation and set frame to still
                mc.gotoAndStop(direction * 4);
                this.animating = false;
                
                // Reduce energy
                this.energy--;
            }
        }
    };
    
    /**
     * The unit displays the attack graphic towards the given
     * board position.
     * 
     * @param {int} boardPos
     * @returns {undefined}
     */
    this.showAttacking = function(boardPos) {
        
        targetPos = map.boardToUnitPos(boardPos);
        
        // Work out which direction for the graphic to face
        if (mc.y > targetPos.y) {
            direction = Direction.UP;
        } else if (mc.x < targetPos.x) {
            direction = Direction.RIGHT;
        } else if (mc.y < targetPos.y) {
            direction = Direction.DOWN;
        } else {
            direction = Direction.LEFT;
        }
        
        // Show the attack frame of the given direction
        mc.gotoAndStop(direction * 4 + 3);
        
    }
    
    /**
     * Shows the normal standing graphic for the unit.
     * 
     * @returns {undefined}
     */
    this.showNormal = function() {
        mc.gotoAndStop(direction * 4);
    }
    
    /**
     * Returns true if the passed unit is in a neighbouring tile
     * (directly above, below or to the side)
     * 
     * @param {Unit} neighbour
     * @returns {Boolean}
     */
    this.isNeighbour = function(neighbour) {
        if (neighbour.boardPos === this.boardPos + 1
                || neighbour.boardPos === this.boardPos - 1
                || neighbour.boardPos === this.boardPos + game.map.columns
                || neighbour.boardPos === this.boardPos - game.map.columns)
            return true;
        else
            return false;
    }
    
    /**
     * Tells the unit it's a new turn. Replenishes energy etc
     * @returns {undefined}
     */
    this.resetTurn = function() {
        if (this.hp > 0)
            this.energy = maxEnergy;
    }
    
    /**
     * Tells a unit it's attacking. Updates energy etc.
     * @returns {undefined}
     */
    this.attack = function() {
        this.energy -= attackEnergy;
        if (this.energy < 0) this.energy = 0;
    }
    
    /**
     * The unit is hurt, so pass its new hp
     * 
     * @param {int} newHp
     * @returns {undefined}
     */
    this.hurt = function(newHp) {
        this.hp = newHp;
        
        // if dead
        if (this.hp < 1) {
            // Show dead sprite
            mc.gotoAndStop(16);
            // Move graphic to background layer
            gfxMgr.removeGraphic(mc, GraphicsLayer.WORLD);
            gfxMgr.addGraphic(mc, GraphicsLayer.BACKGROUND);
            // Remove hp bar
            mc.removeChild(hpBarFill);
            mc.removeChild(hpBarBackground);
        } else {
            updateHpBar();
        }
    }
    
    function updateHpBar() {
        hpBarFill.scale.x = self.hp / 100;
    }
}