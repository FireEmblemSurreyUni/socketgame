"use strict";

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Ryan Pridgeon
 */

/**
 * The Camera class provides a simulated camera to the Game, and
 * provides functions for panning the camera.
 * 
 * @param {Game} gamePass
 * @returns {Camera}
 */
function Camera(gamePass) {
    var self = this;
    var speed = 8;
    var game = gamePass;
    var x = 0; var y = 0; // private
    var destX = 0; var destY = 0;
    var timer = 0;
    var moveRate = 0.1;
    this.x = 0; this.y = 0; // public access
    var pad = 50;

    var gameWidth = game.map.columns * game.map.tileSize;
    var gameHeight = game.map.rows * game.map.tileSize;
    
    /**
     * Updates the graphics objects based on the camera x,y
     */
    this.update = function() {
        // If animating a movement
        if (timer > 0) {
            x += Math.floor((destX - x) * moveRate);
            y += Math.floor((destY - y) * moveRate);
            checkBounds();
            timer--;
        }
        // Move graphicsMgr container to simulate camera position
        game.graphicsMgr.container.x = -x;
        game.graphicsMgr.container.y = -y;
        self.x = x;
        self.y = y;
    };

    /**
     * This pans the camera down.
     */
    this.panDown = function() {
        // Move cam position
        y += speed;

        // If out of bounds, set to max
        if (y > gameHeight - game.STAGE_HEIGHT)
            y = gameHeight - game.STAGE_HEIGHT;
    };

    /**
     * This pans the camera up.
     */
    this.panUp = function() {
        // Move cam position
        y -= speed;

        // If out of bounds, set to min
        if (y < 0)
            y = 0;
    };

    /**
     * This pans the camera right.
     */
    this.panRight = function() {
        // Move cam position
        x += speed;

        // If out of bounds, set to max
        if (x > gameWidth - game.STAGE_WIDTH)
            x = gameWidth - game.STAGE_WIDTH;
    };

    /**
     * This pans the camera left.
     */
    this.panLeft = function() {
        // Move cam position
        x -= speed;

        // If out of bounds, set to min
        if (x < 0)
            x = 0;
    };
    
    function checkBounds() {
        // If out of bounds, set to max
        if (y > gameHeight - game.STAGE_HEIGHT)
            y = gameHeight - game.STAGE_HEIGHT;
        // If out of bounds, set to min
        if (y < 0)
            y = 0;
        // If out of bounds, set to max
        if (x > gameWidth - game.STAGE_WIDTH)
            x = gameWidth - game.STAGE_WIDTH;
        // If out of bounds, set to min
        if (x < 0)
            x = 0;
    }
    
    this.isInView = function(pos) {
        return pos.x > x + pad && pos.x < x + game.STAGE_WIDTH - pad
                && pos.y > y + pad && pos.y < y + game.STAGE_HEIGHT - pad;
    }

    /**
     * Tells the camera to move to show the given world position in the centre of the screen.
     * 
     * @param {{x,y}} pos
     * @returns {undefined}
     */
    this.moveTo = function(pos) {
        
        if (!this.isInView(pos)) {
            destX = pos.x - game.STAGE_WIDTH / 2;
            destY = pos.y - game.STAGE_HEIGHT / 2;
            timer = 20;
        }
    };
    
    /**
     * Shifts the camera by the given x,y
     * 
     * @param {type} x
     * @param {type} y
     * @returns {undefined}
     */
    this.shift = function(sx, sy) {
        x += sx;
        y += sy;
        
        checkBounds();
    }
}

