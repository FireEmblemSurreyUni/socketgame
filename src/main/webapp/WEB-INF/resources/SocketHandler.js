"use strict";

/**
 * A helper wrapper for the native websocket implementation.
 * 
 * Allows binding of functions to specified types or a global context for easier use.
 * 
 * @param {String} socketURL
 * @param {String} initialMode
 * @param {String} onopen Stringified message to send to the server onOpen, has to be specified in the constructor, as that is the start of the socket connection.
 * @returns {SocketHandler}
 */
var SocketHandler =  function(socketURL, initialMode, onopen) {
  
  var socket = new WebSocket(socketURL);
  // bind the onopen function
  socket.onopen = function() {
    socket.send(onopen());
  };
  var globalFlag = "global";
  var handlerMode = initialMode;
  var registry = {};
  
  /**
   * Initialises a registry mode. Two will be specified at creation, global and the one specified in the constructor.
   * 
   * Binding a new function to a new mode will automatically create it in the registry.
   * 
   * @param {String} mode
   */
  function initRegistryMode(mode) {
    registry[mode] = {callbacks: []};
    console.log(JSON.stringify(registry));
  }
  
  /**
   * Message processing function to check that a message exists
   * 
   * TODO: Should probably add some kind of error handling or something...
   * 
   * @param {type} func
   * @param {type} data
   * @returns {undefined}
   */
  function processMessage(func, data) {
    // If the function exists, then call it.
    if (typeof func !== "undefined") {
      func(data);
    }
  }
  
  // Initialise the global callbacks and the initial mode.
  // Global actions will always be called first.
  initRegistryMode(globalFlag);
  initRegistryMode(initialMode);
  
  /**
   * Switch modes for processing bound functions
   * 
   * @param {String} mode
   * @returns {SocketHandler} Can be chained.
   */
  this.switchMode = function(mode) {
    handlerMode = mode;
    return this;
  };
  
  /**
   * Bind a particular handler function to the websocket message type
   * 
   * @param {String} mode The mode that the handler should be used in
   * @param {String} type The type of message that the handler uses
   * @param {function} callback The function that is called when that type message is received.
   * @returns {SocketHandler} Can be chained.
   */
  this.bind = function(mode, type, callback) {
    // if the mode has not been added yet, then add it.
    if (typeof registry[mode] === "undefined") {
      initRegistryMode(mode);
    }
    // Update or add the registry.
    registry[mode].callbacks[type] = callback;
    // return this for chaining.
    return this;
  };
  
  /**
   * Wrapper for default websocket send with auto stringify
   * @param {object} data
   * @returns {SocketHandler} Can be chained.
   */
  this.send = function(data) {
    socket.send(JSON.stringify(data));
    return this;
  };
  
  /**
   * Wrapper for default websocket onclose
   * @param {object} toBind The function to be bound to the onclose event
   * @returns {SocketHandler} Can be chained.
   */
  this.onclose = function(toBind) {
    socket.onclose = toBind;
    return this;
  };
  
  /**
   * Wrapper for default websocket onerror
   * @param {object} toBind The function to be bound to the onerror event
   * @returns {SocketHandler} Can be chained.
   */
  this.onerror = function(toBind) {
    socket.onerror = toBind;
    return this;
  };
  
  /**
   * Set up the default action for the socket to take when it recieves a message
   * @param {String} event The message sent by the server
   */
  socket.onmessage = function(event) {
    var message = JSON.parse(event.data);
    processMessage(registry[globalFlag].callbacks[message.type], message);
    processMessage(registry[handlerMode].callbacks[message.type], message);
  };
  
  return this;
};