package com.com3014.socketgame.dao;

import com.com3014.socketgame.model.GameStatistics;
import com.com3014.socketgame.model.UserDto;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

/**
 * Database access class. Allows data access through java functions.
 * 
 * @author stephenlarkin
 */
public class PostgresDao extends JdbcDaoSupport {
  
  private DataSource dataSource;
  
  /**
   * Mapper class for the user statistics.
   */
  private static final class StatisticsMapper implements RowMapper<GameStatistics> {

    @Override
    public GameStatistics mapRow(ResultSet rs, int rowNum) throws SQLException {
      GameStatistics gs = new GameStatistics(rs.getInt("games_played"), rs.getInt("games_won"));
      return gs;
    }
  }
  
  public PostgresDao(DataSource dataSource) {
    this.dataSource = dataSource;
    System.out.println("ds " + dataSource);
  }
  
  @PostConstruct
	private void initialize() {
		setDataSource(this.dataSource);
	}
  
  /**
   * Inserts a new user into the database and inserts any other rows that the user may be connected to.
   * 
   * @param user The user details to use for the new user.
   * @return boolean If the user has successfully been created in the database
   */
  public boolean createUser(UserDto user) {
    boolean result = true;
    try {
      JdbcTemplate jdbc = new JdbcTemplate(dataSource);
      jdbc.update("INSERT INTO game.users (email, username, pass_hash, date_created, date_updated) VALUES (?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)"
        , new Object[] {user.getEmail(), user.getUsername(), user.getPassword()}
      );
      jdbc.update("INSERT INTO game.user_roles (u_id, role) VALUES ((SELECT id FROM game.users WHERE LOWER(email) = LOWER(?)), 'ROLE_USER')"
        , new Object[] {user.getEmail()}
      );
      jdbc.update("INSERT INTO game.user_stats (u_id, games_won, games_played) VALUES ((SELECT id FROM game.users WHERE LOWER(email) = LOWER(?)), 0, 0)"
        , new Object[] {user.getEmail()}
      );
    } catch (Exception e) {
      result = false;
      System.out.println("Error adding user to database");
    }
    return result;
  }
  
  /**
   * Gets the games won and played for a specific user
   * 
   * @param userid The id of the user to get the stats for
   * @return GameStatistics The statistics object for that user.
   */
  public GameStatistics getGamesWonAndTotal(int userid) {
    JdbcTemplate jdbc = new JdbcTemplate(dataSource);
    GameStatistics stats;
    stats = (GameStatistics)jdbc.queryForObject("SELECT us.games_won, us.games_played FROM user_stats us WHERE us.u_id = ?"
      , new Object[] {userid}
      , new StatisticsMapper()
    );
    return stats;
  }
  
  /**
   * Gets the games won and played for a specific user
   * 
   * @param name The username of the user to retrieve the stats for
   * @return GameStatistics The statistics object for that user.
   */
  public GameStatistics getGamesWonAndTotal(String name) {
    JdbcTemplate jdbc = new JdbcTemplate(dataSource);
    GameStatistics stats;
    stats = (GameStatistics)jdbc.queryForObject("SELECT us.games_won, us.games_played FROM users u JOIN user_stats us ON us.u_id = u.id WHERE u.username = ?"
      , new Object[] {name}, new StatisticsMapper());
    return stats;
  }
  
  /**
   * Updates the games won and played for the specified user.
   * 
   * @param name The username of the user
   * @param win If the user has won or not
   */
  public void updateGamesWonLost(String name, boolean win) {
    int winningInc = 0;
    if (win) {
      winningInc = 1;
    }
    JdbcTemplate jdbc = new JdbcTemplate(dataSource);
    jdbc.update("UPDATE game.user_stats SET games_won = games_won + ?, games_played = games_played + 1 WHERE u_id = (SELECT id FROM game.users WHERE username = ?)"
      , new Object[] {winningInc, name}
    );
  }
  
//  public UserDetail getUserDetail(int userid) {
//    
//  }
  
}
