/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.com3014.socketgame.service;

import com.com3014.socketgame.model.client.ClientMgr;
import com.com3014.socketgame.model.client.iClient;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * The ClientMgrService is a service that provides access to the ClientMgr model.
 * 
 * @author Ryan
 */
@Service
@EnableScheduling
public class ClientMgrService {
    
    /** The ms delay between ticks for checking ping */
    public static final long TICK_RATE = 1000;
    
    /** The ClientMgr model */
    private final ClientMgr mgr = new ClientMgr();
    
    /**
     * Call this to add a new client to the client mgr
     * @param client 
     * @param name - the username, if "null", guest is assumed
     */
    public void addClient(iClient client, String name) {
        synchronized (mgr) {
            mgr.addClient(client, name);
        }
    }
    
    /**
     * Call this when a client has finished a game
     * @param client 
     */
    public void clientEndGame(iClient client) {
        synchronized (mgr) {
            mgr.gameEnd(client);
        }
    }
    
    /**
     * Called continuously on an interval to check client pings,
     * to see if any users have timed out.
     */
    @Scheduled(fixedDelay=1000)
    public void checkPing() {
        //System.out.println("checkping");
        // Call ping() on client manager
        synchronized (mgr) {
            try {
                mgr.ping();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
}
