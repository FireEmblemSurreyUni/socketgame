/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.com3014.socketgame.service;

import com.com3014.socketgame.model.MessageTypes;
import com.com3014.socketgame.model.client.Message;
import com.com3014.socketgame.model.client.iClient;
import com.com3014.socketgame.model.client.iClientListener;
import com.com3014.socketgame.model.game.GameModel;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * This is a service for a game between 2 players. It's a prototype bean
 * so a new instance is created for each game that is initialised.
 * 
 * It implements iClientListener to listen for calls from the clients.
 *
 * @author Ryan
 */
@Component
@Scope("prototype")
public class GameService implements iClientListener {
    
    @Autowired
    ClientMgrService clientMgrService;
    
    @Autowired
    UserService userService;
    
    /** The game model */
    private final GameModel gameModel = new GameModel(  );
    
    /** The iClients playing each other on this game */
    private iClient client1;
    private iClient client2;
    
    /** The iClientListener listening for messages from the clients (probably ClientMgr) */
    iClientListener listener;
    
    /**
     * Initialise a game with the given player clients.
     * 
     * @param client1
     * @param client2 
     * @param listener
     */
    public void init(iClient client1, iClient client2, iClientListener listener) {
        System.out.println("New game service");
        this.client1 = client1;
        this.client2 = client2;
        this.listener = listener;
        
        // Listen to clients
        client1.setClientListener(this);
        client2.setClientListener(this);
        
        // Send "start game" message to both clients
        Message startGame = new Message(MessageTypes.START_GAME);
        client1.sendMessage(startGame);
        client2.sendMessage(startGame);
        
        // Send game info message to both clients
        Message gameInfo = gameModel.getInfoMessage();
        client1.sendMessage(gameInfo);
        gameInfo.setTeamNumber(1);
        client2.sendMessage(gameInfo);
        
    }
    
    /**
     * Called when the game ends
     */
    public void end() {
        int winner = gameModel.getWinner();
        System.out.println("Game end " + winner);
        
        userService.recordGameToUser(client1.getName(), winner == 0, winner == 1);
        userService.recordGameToUser(client2.getName(), winner == 1, winner == 0);
        
        client1.setClientListener(listener);
        client2.setClientListener(listener);
    }
    
    /**
     * Ends the turn and starts the turn for the next player. Sends out
     * a turn end message to clients.
     */
    public void endTurn() {
        // End turn in model
        gameModel.endTurn();
        
        // Make an end turn message
        Message turnMsg = new Message(MessageTypes.GAME_SERVER_END_TURN);
        turnMsg.setTeamNumber(gameModel.getTurn());
        
        // Send end turn to clients
        client1.sendMessage(turnMsg);
        client2.sendMessage(turnMsg);
    }

    /**
     * Called when a message is received by a client.
     * 
     * From iClientListener
     * 
     * @param client
     * @param message 
     */
    @Override
    public void messageReceived(iClient client, Message message) {
        // Find out which team number the client is
        int team;
        if (client == client1) team = 0;
        else if (client == client2) team = 1;
        else return;
        
        // Client wants to end their turn
        if (message.getType() == MessageTypes.GAME_CLIENT_END_TURN) {
            
            // If it's actually their turn then end the turn
            if (gameModel.getTurn() == team) {
                endTurn();
            }
            
        // Client wants to move a unit
        } else if (message.getType() == MessageTypes.GAME_CLIENT_MOVE) {
            
            // Attempt to move unit and check if valid and successful
            if (gameModel.moveUnit(team, message.getUnitID(), message.getBoardPos())) {
                
                // Create move message to send to clients
                Message moveMsg = new Message(MessageTypes.GAME_SERVER_MOVE);
                moveMsg.setUnitID(message.getUnitID());
                moveMsg.setBoardPos(message.getBoardPos());
                moveMsg.setActionID(message.getActionID());
                
                client1.sendMessage(moveMsg);
                client2.sendMessage(moveMsg);
                
            } else {
                // Move isn't valid, send a reject message back to the client
                Message reject = new Message(MessageTypes.GAME_INVALID_ACTION);
                reject.setActionID(message.getActionID());
                
                client.sendMessage(reject);
            }
            
        // Client wants to attack with a unit
        } else if (message.getType() == MessageTypes.GAME_CLIENT_ATTACK) {
            
            // Check atack is valid
            if (gameModel.isAttackValid(team, message.getUnitID(), message.getBoardPos())) {
                
                // Perform attack in game model, get list of units that were damaged
                ArrayList<GameModel.HurtUnit> hurtUnits = 
                        gameModel.attackUnit(team, message.getUnitID(), message.getBoardPos());
                
                // Create attack message to send to clients
                Message attackMsg = new Message(MessageTypes.GAME_SERVER_ATTACK);
                attackMsg.setUnitID(message.getUnitID());
                attackMsg.setBoardPos(message.getBoardPos());
                attackMsg.setActionID(message.getActionID());
                
                // Add list of units that were damaged to message
                for (int i = 0; i < hurtUnits.size(); i++)
                    attackMsg.addHurtUnit(hurtUnits.get(i).getUnitID(), hurtUnits.get(i).getHp());
                
                client1.sendMessage(attackMsg);
                client2.sendMessage(attackMsg);
                
            } else {
                // Move isn't valid, send a reject message back to the client
                Message reject = new Message(MessageTypes.GAME_INVALID_ACTION);
                reject.setActionID(message.getActionID());
                
                client.sendMessage(reject);
            }
            
        } else {
        
            // Pass to other listener
            synchronized (listener) {
                listener.messageReceived(client, message);
            }
            
        }
        
        // If there's a winner this means the game has ended
        if (gameModel.getWinner() != -1) {
            // Make an end game message containing the winning team number
            Message endMsg = new Message(MessageTypes.GAME_ENDED);
            endMsg.setTeamNumber(gameModel.getWinner());
            
            // Send to clients
            client1.sendMessage(endMsg);
            client2.sendMessage(endMsg);
            
            // end game
            end();
            
            clientMgrService.clientEndGame(client1);
            clientMgrService.clientEndGame(client2);
        }
        
    }

    /**
     * Called when a client disconnects.
     * 
     * From iClientListener
     * 
     * @param client 
     */
    @Override
    public void clientDisconnect(iClient client) {
        //System.out.println("GameService.clientDisconnect " + client.getName());
        // End the game
        end();
        
        // cancel message
        Message cancelMsg = new Message(MessageTypes.GAME_CANCELLED);
        
        // Tell other client the game is cancelled
        if (client == client1) {
            client2.sendMessage(cancelMsg);
            clientMgrService.clientEndGame(client2);
        } else {
            client1.sendMessage(cancelMsg);
            clientMgrService.clientEndGame(client1);
        }
        
        // Pass to other listener (ClientMgr)
        synchronized (listener) { 
            listener.clientDisconnect(client);
        }
    }
}
