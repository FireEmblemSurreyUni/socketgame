/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.com3014.socketgame.service;

import com.com3014.socketgame.dao.PostgresDao;
import com.com3014.socketgame.model.UserDto;
import java.util.ArrayList;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Dummy user service which creates a fake user to return for logging in / security etc
 *
 * @author Ryan
 */
@Service(value="userService")
public class UserService implements UserDetailsService {

  @Autowired
  private DataSource dataSource;
  
  private static PostgresDao dao = null;
  
  public UserService() {
      
    //dao = new PostgresDao(dataSource);
  }
  
    /**
     * Called by authentication manager to load a user with the given username.
     * Returns an implementation of UserDetails interface.
     * 
     * @param username
     * @return UserDetails
     * @throws UsernameNotFoundException 
     */
    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        System.out.println("loadUserByUsername " + name);
        
        ArrayList<SimpleGrantedAuthority> auth = new ArrayList<>();
        auth.add(new SimpleGrantedAuthority("ROLE_USER"));
        return new User(name, "changeme", auth);
    }
    
    /**
     * Call this function to register a new user to the system. Pass the
     * DTO for the new user.
     * 
     * http://www.baeldung.com/registration-with-spring-mvc-and-spring-security
     * 
     * @param user The user to register to the database
     * @return true if successful
     */
    public boolean registerUser(UserDto user) {
        System.out.println("registerUser " + user.getUsername());
        
        // If no dao, initialise one
        if (dao == null)
            dao = new PostgresDao(dataSource);
        
        boolean result = false;
        result = dao.createUser(user);
        return result;
    }
    
    /**
     * Returns the list of high scores. The return value is an array list
     * where each element is an instance of HighScore, containing a name,
     * wins, and gamesPlayed. The list is ordered, where element 0 is the 
     * best score, and subsequent elements are the next in rankings.
     * 
     * Example usage;
     * ArrayList<UserService.HighScore> highScores = userService.getHighScores();
     * int nameOfBestScore = highScores.get(0).getName();
     * int nameOfSecondBest = highScores.get(1).getName();
     * 
     * // To print all high scoring users
     * for (i = 0; i < highScores.size(); i++) {
     *   print(highScores.get(i).getName());
     *   print(highScores.get(i).getWins());
     * }
     * 
     * @return ranked list of HighScore objects
     */
    public ArrayList<HighScore> getHighScores() {
        ArrayList<HighScore> list = new ArrayList<>();
        list.add(new HighScore("ryan", 50500, 50500));
        list.add(new HighScore("bob", 0, 500));
        list.add(new HighScore("jim", 0, 50000));
        
        return list;
    }
    
    public class HighScore {
        
        String name;
        int wins;
        int gamesPlayed;

        public HighScore(String name, int wins, int gamesPlayed) {
            this.name = name;
            this.wins = wins;
            this.gamesPlayed = gamesPlayed;
        }

        public String getName() {
            return name;
        }

        public int getWins() {
            return wins;
        }

        public int getGamesPlayed() {
            return gamesPlayed;
        }
        
                 
    }
    
    /**
     * When a game ends, this is called to record the game to the user's stats.
     * If the user won, win is true. If the user lost, lose is true.
     * 
     * @param name
     * @param win
     * @param lose 
     */
    public void recordGameToUser(String name, boolean win, boolean lose) {
        System.out.println("UserService.recordGameToUser " + name);
        if (dao == null)
            dao = new PostgresDao(dataSource);
        dao.updateGamesWonLost(name, win);
    }
    
}
