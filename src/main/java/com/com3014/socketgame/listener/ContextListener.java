/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.com3014.socketgame.listener;

import com.com3014.socketgame.service.ClientMgrService;
import com.com3014.socketgame.websocket.SocketEndpoint;
import com.com3014.socketgame.websocket.SocketEndpointConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.websocket.DeploymentException;
import javax.websocket.server.ServerContainer;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The ContextListener executes functions when the web application context
 * is initialised and destroyed. During intialisation it's used to configure
 * websockets.
 * 
 * Based on code from https://github.com/cemartins/websocket-test/blob/master/websocket-server/src/main/java/org/juffrou/test/websocket/MyApplication.java
 *
 * @author Ryan Pridgeon
 */
public class ContextListener implements ServletContextListener {
    
    /**
     * Called when the Spring context is initialised. Sets up websocket endpoint configuration.
     * 
     * @param sce 
     */
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("Context initialised!");
        
        ServletContext context = sce.getServletContext();
        
        final ServerContainer container = (ServerContainer)context.getAttribute("javax.websocket.server.ServerContainer");
        
        // Try to add endpoint config
        try {
            container.addEndpoint(new SocketEndpointConfig(SocketEndpoint.class, "/wstest"));
            
            System.out.println("Endpoint config succesfully added " + container.toString());
        } catch (DeploymentException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Called when the Spring context is destroyed. Updates the state to allow
     * destroyed() to return true.
     * 
     * @param sce 
     */
    public void contextDestroyed(ServletContextEvent sce) {
        
        System.out.println("Context destroyed");
    }
}
