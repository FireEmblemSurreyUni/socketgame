/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.com3014.socketgame.websocket;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.websocket.Decoder;
import javax.websocket.Encoder;
import javax.websocket.Extension;
import javax.websocket.server.ServerEndpointConfig;

/**
 * Configuration for the websocket endpoints. Sets up a given endpoint class at
 * the given path.
 * 
 * Based on code from https://github.com/cemartins/websocket-test/blob/master/websocket-server/src/main/java/org/juffrou/test/websocket/MyEndpointConfig.java
 *
 * @author Ryan Pridgeon
 */
public class SocketEndpointConfig implements ServerEndpointConfig {

    private final String path;
    private final Class<?> endpointClass;

    public SocketEndpointConfig(Class<?> endpointClass, String path) {
        this.endpointClass = endpointClass;
        this.path = path;
    }

    @Override
    public List<Class<? extends Encoder>> getEncoders() {
        return Collections.emptyList();
    }

    @Override
    public List<Class<? extends Decoder>> getDecoders() {
        return Collections.emptyList();
    }

    @Override
    public Map<String, Object> getUserProperties() {
        return Collections.emptyMap();
    }

    @Override
    public Configurator getConfigurator() {
        return new SpringConfigurator();
    }

    @Override
    public Class<?> getEndpointClass() {
        return endpointClass;
    }

    @Override
    public List<Extension> getExtensions() {
        return Collections.emptyList();
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public List<String> getSubprotocols() {
        return Collections.emptyList();
    }
}
