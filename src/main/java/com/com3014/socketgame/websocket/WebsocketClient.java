/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.com3014.socketgame.websocket;

import com.com3014.socketgame.model.MessageTypes;
import com.com3014.socketgame.model.client.Message;
import com.com3014.socketgame.model.client.iClient;
import com.com3014.socketgame.model.client.iClientListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;
import javax.websocket.Session;

/**
 * WebsocketClient is an implementation of iClient which handles the connection
 * to a client connected by websockets. It's instantiated by SocketEndpoint and
 * uses a websocket Session to communicate with the client. Received messages
 * are parsed from json and sent messages generate json.
 *
 * @author Ryan Pridgeon
 */
public class WebsocketClient implements iClient {
    
    private Object pingLock = new Object();
    private long lastPing;
    
    private String name;
    private int uID;
    
    private iClientListener listener = null;
    
    private Session session;
    
    public WebsocketClient(Session session) {
        this.session = session;
        this.lastPing = System.currentTimeMillis();
    }
    
    /**
     * Called when the endpoint receives a message.
     * 
     * @param messageString 
     */
    synchronized public void messageReceived(String messageString) {
        Message message = new Message(0);
        
        // Create a json parser to parse the string
        JsonParser parser;
        try {
            parser = Json.createParser(new ByteArrayInputStream(messageString.getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        
        // Parse loop through json
        while (parser.hasNext()) {
            Event event = parser.next();
            //System.out.println(event.toString());
            
            if (event == Event.KEY_NAME)
            {
                String keyName = parser.getString();
                //System.out.println("Parsing " + keyName);
                
                // Would use switch here, but we're nested in a for loop and "break" wouldn't play nice
                if (keyName.equals("type")) {
                    parser.next();
                    int type = parser.getInt();
                    message.setType(type);
                } else if (keyName.equals("name")) {
                    parser.next();
                    String name = parser.getString();
                    message.setName(name);
                } else  if (keyName.equals("uid")) {
                    parser.next();
                    int uid = parser.getInt();
                    message.setUID(uid);
                } else if (keyName.equals("status")) {
                    parser.next();
                    String status = parser.getString();
                    message.setStatus(status);
                } else if (keyName.equals("gameId")) {
                    parser.next();
                    int gameId = parser.getInt();
                    message.setOpponentID(gameId);
                } else if (keyName.equals("opponentUid")) {
                    parser.next();
                    int opponentUid = parser.getInt();
                    message.setOpponentID(opponentUid);
                } else if (keyName.equals("message")) {
                    parser.next();
                    String msg = parser.getString();
                    message.setMessage(msg);
                } else if (keyName.equals("unitId")) {
                    parser.next();
                    int unitId = parser.getInt();
                    message.setUnitID(unitId);
                } else if (keyName.equals("actionId")) {
                    parser.next();
                    int actionId = parser.getInt();
                    message.setActionID(actionId);
                } else if (keyName.equals("boardPos")) {
                    parser.next();
                    int boardPos = parser.getInt();
                    message.setBoardPos(boardPos);
                }
            }
        }
        
        // Ping received
        if (message.getType() == MessageTypes.PING_PONG) {
            synchronized (pingLock) {
                //System.out.println("Ping received");
                this.lastPing = System.currentTimeMillis();
            }
            return; // Don't bother parsing anywhere else
        }
        
        System.out.println("RECEIVED : WebsocketClient " + this.name + " : " + message.toString());
        
        synchronized (listener) {
            listener.messageReceived(this, message);
        }
    }
    
    synchronized public void end() {
        synchronized (listener)  {
            //System.out.println("WebsocketClient.end");
            listener.clientDisconnect(this);
        }
    }
    
    /**
     * Close the connection to this client
     */
    synchronized public void close() {
        try {
            session.close();
            //System.out.println("WebsocketClient.close closed");
        } catch(Exception e) { e.printStackTrace(); }
    }
    
    /**
     * Set the listener which will listen for events such as messages from this client
     * 
     * @param listener 
     */
    synchronized public void setClientListener(iClientListener listener) {
        this.listener = listener;
    }
    
    /**
     * Send the given Message to the client.
     * 
     * @param message 
     */
    synchronized public void sendMessage(Message message) {
        
        // Create a json generator
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        JsonGenerator generator;
        generator = Json.createGenerator(os);
        
        // Write message properties to json
        generator.writeStartObject().write("type", message.getType());
        if (message.getName() != null)
            generator.write("name", message.getName());
        if (message.getUID() != -1)
            generator.write("uid", message.getUID());
        if (message.getStatus() != null)
            generator.write("status", message.getStatus());
        if (message.getOpponentID() != -1)
            generator.write("opponentUid", message.getOpponentID());
        if (message.getMessage() != null)
            generator.write("message", message.getMessage());
        if (message.getTeamNumber() != -1)
            generator.write("teamNumber", message.getTeamNumber());
        if (message.getBoardWidth() != -1)
            generator.write("boardWidth", message.getBoardWidth());
        if (message.getBoard() != null)
            generator.write("board", message.getBoard());
        if (message.getUnitID() != -1)
            generator.write("unitId", message.getUnitID());
        if (message.getBoardPos() != -1)
            generator.write("boardPos", message.getBoardPos());
        if (message.getActionID() != -1)
            generator.write("actionId", message.getActionID());
        
        // Write userlist to json
        if (message.getUserlist().size() > 0) {
            generator.writeStartArray("userlist");
            for (Message.User listUser : message.getUserlist()) {
                generator.writeStartObject();
                    generator.write("uid", listUser.getUID());
                    generator.write("name", listUser.getName());
                    generator.write("status", listUser.getStatus());
                generator.writeEnd();
            }
            generator.writeEnd();
        }
        
        // Write units to json
        if (message.getUnits().size() > 0) {
            generator.writeStartArray("units");
            for (Message.Unit unit : message.getUnits()) {
                generator.writeStartObject();
                    generator.write("unitId", unit.getUnitID());
                    generator.write("teamNumber", unit.getTeamNumber());
                    generator.write("unitType", unit.getUnitType());
                    generator.write("boardPos", unit.getBoardPos());
                generator.writeEnd();
            }
            generator.writeEnd();
        }
        
        // Write hurt units to json
        if (message.getHurtUnits().size() > 0) {
            generator.writeStartArray("hurtUnits");
            for (Message.HurtUnit unit : message.getHurtUnits()) {
                generator.writeStartObject();
                    generator.write("unitId", unit.getUnitID());
                    generator.write("hp", unit.getHp());
                generator.writeEnd();
            }
            generator.writeEnd();
        }
            
        generator.writeEnd();
        generator.close();
        
        // Try to convert the json generator to a json string
        String messageString = "";
        try {
            messageString = new String(os.toByteArray(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        
        // Try to send the message
        if (message.getType() != MessageTypes.PING_PONG)
            System.out.println("SENDING  : WebsocketClient " + this.name + " : " + messageString);
        try {
            this.session.getBasicRemote().sendText(messageString);
        } catch (IOException e) {
            System.out.println("Failed to send");
            e.printStackTrace();
            return;
        }
    }
    
    
    /**
     * Set the user's name
     * 
     * @param name 
     */
    synchronized public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Set the user ID
     * 
     * @param uID 
     */
    synchronized public void setUID(int uID) {
        this.uID = uID;
    }
    
    /**
     * Get this client's username
     * 
     * @return name
     */
    synchronized public String getName() {
        return name;
    }
    
    /**
     * Get the user ID
     * 
     * @return uid
     */
    synchronized public int getUID() {
        return uID;
    }
    
    /**
     * Get the last time in system time that ping was received from this client
     * 
     * @return system time ms
     */
    synchronized public long getLastPingTime() {
        synchronized (pingLock) {
            return lastPing;
        }
    }
    
    /**
     * Returns false if the socket has been disconnected
     * 
     * @return boolean
     */
    synchronized public boolean isAlive() {
        return session.isOpen();
    }
}
