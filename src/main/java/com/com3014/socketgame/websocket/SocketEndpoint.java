/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.com3014.socketgame.websocket;

import com.com3014.socketgame.service.ClientMgrService;
import javax.websocket.CloseReason;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The SocketEndpoint is the endpoint class for connections via websockets.
 * It handles the connection, disconnection and messages of a websocket connection.
 * The Session is used to communicate messages to the client, and it implements
 * MessageHandler to be able to receive messages from the session. It creates
 * a new WebsocketClient instance and adds it to the ClientMgr service to be managed.
 * It's instantiated as a bean from SpringConfigurator when a websocket client connects.
 * 
 * Based on code from https://github.com/cemartins/websocket-test/blob/master/websocket-server/src/main/java/org/juffrou/test/websocket/MyEndpoint.java
 *
 * @author Ryan Pridgeon
 */
@Component
public class SocketEndpoint extends Endpoint implements MessageHandler.Whole<String> {
    
    /** The autowired reference to the clientMgrService */
    @Autowired
    ClientMgrService clientMgrService;
    
    /** The WebsocketClient instance associated with this endpoint. */
    private WebsocketClient client;
    
    /**
     * When a new connection is opened to a client, this function is called 
     * with the session and endpoint config as parameters.
     * 
     * @param session
     * @param config 
     */
    @Override
    public void onOpen(Session session, EndpointConfig config) {
        
        String name = null;
        
        // If a session principal exists, get the name
        if (session.getUserPrincipal() != null) {
            name = session.getUserPrincipal().getName();
        }
        
        // add this as the message handler
        session.addMessageHandler(this);
        
        // Create new WebsocketClient with the new connection's session
        client = new WebsocketClient(session);
        clientMgrService.addClient(client, name);
    }
    
    @Override
    public void onClose(Session session, CloseReason closeReason) {
        
        System.out.println("SocketEndpoint.close");
        client.end();
    }
    
    @Override
    public void onMessage(String message) {
        
        // Pass message string to WebsocketClient
        client.messageReceived(message);
    }
}
