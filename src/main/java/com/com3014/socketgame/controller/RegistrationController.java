/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.com3014.socketgame.controller;

import com.com3014.socketgame.model.UserDto;
import com.com3014.socketgame.service.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Umesh
 */
@Controller
public class RegistrationController {
    
    @Autowired 
    UserService userService;
    
    /**
     * display the registration form
     * @return 
     */
    @RequestMapping(value="/registration",method=RequestMethod.GET)
    public String showRegistrationForm(){
        return "register";
    }
    
    /**
     * 
     * @param user
     * @param result
     * @param model
     * @param request
     * processes the registration form  
     * @return 
     */
    @RequestMapping(value="/registration",method=RequestMethod.POST)
    public String submitRegistrationForm(@Valid @ModelAttribute("user")UserDto user, BindingResult result, ModelMap model, HttpServletRequest request)
    {
            if (result.hasErrors())
                return "register?error";
            
            if (userService.registerUser(user)) {
                signInUser(user);
                
                return "registerSuccessful";
            } else {
                return "register";
            }         
    }   
   
    private void signInUser(UserDto user) {
        
        Authentication authentication = new UsernamePasswordAuthenticationToken(user.getUsername(),user.getPassword());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
}
