/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.com3014.socketgame.model.game;

/**
 * Models a single unit within the game, for example a soldier.
 *
 * @author Ryan Pridgeon
 */
public class GameUnit {
    
    /** The team number for the team this unit is part of */
    private int teamNumber;
    
    /** Position in the 1d game board string */
    private int boardPos;
    
    /** Health points - if 0, unit is dead */
    private int hp;
    
    /** The unit's type */
    private String type;
    
    /** Energy that is replenished each turn */
    private int maxEnergy;
    
    /** Energy left in this turn; decrements for each tile that is travelled */
    private int energy;
    
    /** Energy lost when attacking */
    private int attackEnergy;
    
    /** Damage that attacks deal to other units */
    private int damage;

    public GameUnit(int teamNumber, int boardPos, String type) {
        this.teamNumber = teamNumber;
        this.boardPos = boardPos;
        this.type = type;
        this.hp = 100;
        
        // Set stats based on unit type
        if (this.type.equals("soldier")) {
            maxEnergy = 6;
            damage = 40;
            attackEnergy = maxEnergy;
        } else if (this.type.equals("scout")) {
            maxEnergy = 10;
            damage = 10;
            attackEnergy = 5;
        }
        
        replenish();
    }
    
    /**
     * Returns true if the given board position is within attacking range of this unit.
     * 
     * @param attackPos
     * @param boardWidth
     * @return 
     */
    public boolean withinAttackRange(int attackPos, int boardWidth) {
        // If to the left
        if (boardPos % boardWidth > 0 && attackPos == boardPos - 1)
            return true;
        // If to the right
        else if (boardPos % boardWidth < boardWidth - 1 && attackPos == boardPos + 1)
            return true;
        // If to the top
        if (attackPos == boardPos - boardWidth)
            return true;
        // If to the bottom
        else if (attackPos == boardPos + boardWidth)
            return true;
        else
            return false;
    }
    
    // The proportion of the damage dealt that depends on the unit's remaining energy
    double proportion = 0.5;
    /**
     * Get the damage amount for an attack by this unit
     * @return 
     */
    public int getDamage() {
        double energyProportion = ((double)this.energy) / ((double)this.maxEnergy);
        double damageProportion = energyProportion * proportion + 1 - proportion;
        int dmg = (int)(damageProportion * (double)this.damage);
        
        return dmg;
    }
    
    /**
     * Tell this unit that it has made an attack
     */
    public void attack() {
        this.energy -= this.attackEnergy;
        
        if (this.energy < 0) this.energy = 0;
    }
    
    /**
     * Hurt this unit's hp by the given damage amount
     * @param damage 
     */
    public void hurt(int damage) {
        this.hp -= damage;
        if (this.hp < 0) this.hp = 0;
    }
    
    /**
     * Call each turn to replenish energy
     */
    public void replenish() {
        energy = maxEnergy;
    }
    
    /**
     * Move the unit to the given board position
     * @param boardPos 
     */
    public void moveTo(int boardPos) {
        this.boardPos = boardPos;
    }

    public int getTeamNumber() {
        return teamNumber;
    }

    public int getBoardPos() {
        return boardPos;
    }

    public int getHp() {
        return hp;
    }

    public String getType() {
        return type;
    }
    
    public void decreaseEnergy(int amount) {
        energy -= amount;
    }
    
    public int getEnergy() {
        return energy;
    }
    
}
