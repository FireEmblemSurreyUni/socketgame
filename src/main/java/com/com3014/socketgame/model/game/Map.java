/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.com3014.socketgame.model.game;

/**
 * Describes a "map", which is the initial layout of the game board
 * and game units for a single game. Number characters in the map such as
 * '0' or '4' describe game board "tiles" like walls or grass, and letters such
 * as 'a' or 'b' describe the starting positions of units.
 *
 * @author Ryan
 */
public class Map {
    
    private static final int NUM_MAPS = 2;
    
    private String map;
    private int width;
    
    /**
     * Returns a map randomly from the selection.
     * 
     * @return 
     */
    public static Map getRandom() {
        return new Map((int)(Math.random() * NUM_MAPS));
    }
    
    public String getMap() {
        return map;
    }

    public int getWidth() {
        return width;
    }
    
    /**
     * Creates the map from the selection given by index
     * @param index 
     */
    private Map(int index) {
        switch (index) {
            case 0:
                this.map = 
                        "44444444444444444444" +
                        "40000000000004000004" + 
                        "40000000000004000004" + 
                        "40000000000004000004" + 
                        "40000040a0b004000004" + 
                        "40000040000000000004" + 
                        "40000040000000000004" + 
                        "40000040000000000004" + 
                        "44444444444444444444";
                this.width = 20;
                break;
                
            case 1:
                this.map =
                        "444444444444444444444444444444" +
                        "400010000000000002000400000004" +
                        "400000000010000000000400b00b04" +
                        "4000000000000000010004000b0004" +
                        "40001000555555555500040d00d004" +
                        "400000005000000000020000000004" +
                        "400030005020000565000000010004" +
                        "400000005000400100000000000004" +
                        "400100005000400000000500000004" +
                        "400000005000444444000501000004" +
                        "400000000000000000000500020004" +
                        "400000000000001000000500000004" +
                        "400010004444444444444400001004" +
                        "400000000000040000000510000004" +
                        "401000000200000000020500000004" +
                        "40c000c05000044440000500000004" +
                        "4000a0005000040040000500100004" +
                        "40a000a05020040000000000000004" +
                        "400000005000000000020000000004" +
                        "444444444444444444444444444444";
                this.width = 30;
                break;
                
            default:
                this.map =
                        "444444444444444444444444444444" +
                        "400000000000000000000000000004" +
                        "400000000000000000000000000004" +
                        "400000000000000000000000000004" +
                        "400000000000000000000000000004" +
                        "400000000000000000000000000004" +
                        "400000000000000000000000000004" +
                        "400000000000000000000000000004" +
                        "400000000000000000000000000004" +
                        "400000000000000000000000000004" +
                        "400000000000000000000000000004" +
                        "400000000000000000000000000004" +
                        "400000000000000000000000000004" +
                        "400000000000000000000000000004" +
                        "400000000000000000000000000004" +
                        "400000000000000000000000000004" +
                        "400000000000000000000000000004" +
                        "400000000000000000000000000004" +
                        "400000000000000000000000000004" +
                        "444444444444444444444444444444";
                this.width = 30;
                break;
        }
    }
}
