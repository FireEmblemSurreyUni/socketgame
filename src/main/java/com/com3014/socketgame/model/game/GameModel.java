/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.com3014.socketgame.model.game;

import com.com3014.socketgame.model.MessageTypes;
import com.com3014.socketgame.model.client.Message;
import java.util.ArrayList;

/**
 * Models a game between 2 players. Has functions for making
 * player moves or information about the state of the game.
 *
 * @author rp00091
 */
public class GameModel {
    
    public final int PLAYER_1 = 0;
    public final int PLAYER_2 = 1;
    
    /** When the game has ended, this is set to the winning team number */
    private int winner = -1;
    
    /** The game board / tile map */
    private int boardWidth;    
    private String board = new String();
    
    /** List of game units */
    private ArrayList<GameUnit> units = new ArrayList<>();
    
    /** Pathfinder instance */
    private Pathfinder pathfinder;
    
    /** Whose turn it currently is */
    private int turn = PLAYER_1;
    
    /**
     * Construct a new game model with the given map information.
     * The map is a string representing the game map, where each
     * character is a tile. For example
     * "1111
     *  10a1
     *  1001
     *  1111", mapWidth = 4
     * 
     * The 0 and 1 correspond to tiles (walls or ground), the a
     * is a game unit.
     * 
     * @param map
     * @param mapWidth 
     */
    public GameModel() {
        
        // get a map randomly
        Map map = Map.getRandom();
        
        this.boardWidth = map.getWidth();
        
        // Loop through map and create units and board
        for (int i = 0; i < map.getMap().length(); i++) {
            
            char mapChar = map.getMap().charAt(i);
            
            // Interpret map tile character
            switch (mapChar) {
                case 'a': // Player 1 soldier
                    addUnit(PLAYER_1, i, "soldier");
                    board += '0';
                    break;
                    
                case 'b': // Player 2 solider
                    addUnit(PLAYER_2, i, "soldier");
                    board += '0';
                    break;
                    
                case 'c': // Player 1 scout
                    addUnit(PLAYER_1, i, "scout");
                    board += '0';
                    break;
                    
                case 'd': // Player 2 scout
                    addUnit(PLAYER_2, i, "scout");
                    board += '0';
                    break;
                    
                default: // No unit, just place tile
                    board += mapChar;
                    break;
            }
        }
        
        pathfinder = new Pathfinder(board, boardWidth);
        
    }
    
    /**
     * Add a unit to the game with the given team, 
     * @param teamNumber
     * @param boardPos
     * @param type 
     */
    private void addUnit(int teamNumber, int boardPos, String type) {
        
        GameUnit unit = new GameUnit(teamNumber, boardPos, type);
        
        units.add(unit);
    }
    
    /**
     * The given team moves the given unit to the given position. If move
     * is invalid, returns false. Returns true on successful move.
     * 
     * @param teamNumber
     * @param unitID
     * @param boardPos 
     * @return true if successful, false if invalid
     */
    public boolean moveUnit(int teamNumber, int unitID, int boardPos) {
        
        GameUnit unit = units.get(unitID);
        
        // If not on the user's team, return false for invalid move
        if (unit.getTeamNumber() != teamNumber) return false;
        // If unit is dead, invalid move
        if (unit.getHp() < 1) return false;
        
        int path;
        try {
            path = pathfinder.findPath(unit.getBoardPos(), boardPos, unit.getEnergy(), units);
            
            // If path length more than 0, path found
            if (path > 0) {
                // Move unit and decrease energy by path length
                unit.moveTo(boardPos);
                unit.decreaseEnergy(path);
                return true; // success

            } else { // No path found, invalid move
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        
    }
    
    /**
     * Checks if attacking the given position with the given unit by the given
     * team is a valid move.
     * 
     * @param teamNumber
     * @param unitID
     * @param boardPos
     * @return true if valid, false if invalid
     */
    public boolean isAttackValid(int teamNumber, int unitID, int boardPos) {
        GameUnit unit = units.get(unitID);
        
        // If unit's dead, invalid move
        if (unit.getHp() < 1) return false;
        // If not on the user's team, invalid move
        else if (unit.getTeamNumber() != teamNumber) return false;
        // Otherwise, return whether the unit is in range
        else return unit.withinAttackRange(boardPos, boardWidth);
    }
    
    /**
     * The given team attacks the given position with the given unit.
     * 
     * @param teamNumber
     * @param unitID
     * @param boardPos
     * @return list of info on units that are hurt in the attack.
     */
    public ArrayList<HurtUnit> attackUnit(int teamNumber, int unitID, int boardPos) {
        ArrayList<HurtUnit> victimList = new ArrayList<>();
        
        // Find units that are within attack range
        for (int i = 0; i < units.size(); i++) {
            // If on the attack position and not dead
            if (units.get(i).getBoardPos() == boardPos && units.get(i).getHp() > 0) {
                
                // Deal damage from attacking unit
                int damage = units.get(unitID).getDamage();
                units.get(i).hurt(damage);
                // Update attacking unit's stats
                units.get(unitID).attack();
                // Add hurt unit to list
                victimList.add(new HurtUnit(i, units.get(i).getHp()));
            }
        }
        
        // Check if the game is over
        int numberOfLivingUnits[] = { 0, 0 };
        // For eac hunit
        for (GameUnit uniti : units) {
            // If alive
            if (uniti.getHp() > 0) {
                numberOfLivingUnits[uniti.getTeamNumber()]++; // Add to live units counter for its team
            }
        }
        
        // If all of team 0 is dead
        if (numberOfLivingUnits[0] < 1) {
            winner = 1;
        // If all of team 1 is dead
        } else if (numberOfLivingUnits[1] < 1) {
            winner = 0;
        }
        
        return victimList;
    }
    
    /**
     * Ends the turn and starts the turn for the next player
     */
    public void endTurn() {
        if (this.turn == PLAYER_1)
            this.turn = PLAYER_2;
        else
            this.turn = PLAYER_1;
        
        for (GameUnit unit : units)
            unit.replenish();
    }
    
    /**
     * Returns the team number of whose turn it currently is.
     * 
     * @return teamNumber
     */
    public int getTurn() {
        return this.turn;
    }
    
    /**
     * Makes a game info message to send to clients when they load the game. Contains
     * the board and list of units.
     * 
     * @return Message
     */
    public Message getInfoMessage() {
        Message gameInfo = new Message(MessageTypes.GAME_INFO);
        gameInfo.setTeamNumber(0);
        gameInfo.setBoardWidth(boardWidth);
        gameInfo.setBoard(board);
        
        for (int i = 0; i < units.size(); i++)
            gameInfo.addUnit
                    (i, units.get(i).getTeamNumber(), units.get(i).getType(), units.get(i).getBoardPos());
        
        return gameInfo;
    }
    
    /**
     * If the game is still running, returns -1. Otherwise, returns the winning team number.
     * @return team number
     */
    public int getWinner() {
        return winner;
    }
    
    /**
     * Gives information about a unit that was hurt in an attack move.
     */
    public class HurtUnit {
        private int unitID;
        private int hp;

        public HurtUnit(int unitID, int hp) {
            this.unitID = unitID;
            this.hp = hp;
        }

        public int getUnitID() {
            return unitID;
        }

        public int getHp() {
            return hp;
        }
    }
}
