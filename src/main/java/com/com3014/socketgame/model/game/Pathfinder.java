/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.com3014.socketgame.model.game;

import java.util.ArrayList;

/**
 * The Pathfinder class finds valid paths between tiles on the game board,
 * the length of the paths and whether there is a possible path. Used for validating
 * moves in the game, and reducing unit's energy from travelling (path length).
 * 
 * Implemented using a modified A* search algorithm
 * reference;
 * http://en.wikipedia.org/wiki/A*_search_algorithm
 *
 * @author Ryan
 */
public class Pathfinder {
    
    /** Tiles below this value are considered passable. Tiles above
     * or below are considered solid / impassable. */
    private final static char SOLID = '4';
    private final boolean[] boardCollisionMap;
    private final int boardWidth;
    private final int boardHeight;
    private final Node[] nodes;
    
    /**
     * Construct a pathfinder instance. Give the board tile string and the width
     * of the board as parameters.
     * 
     * @param board
     * @param boardWidth 
     */
    public Pathfinder(String board, int boardWidth) {
        int i;
        
        // Set board dimensions
        this.boardWidth = boardWidth;
        this.boardHeight = board.length() / boardWidth;
        
        // Set board collision map
        this.boardCollisionMap = new boolean[board.length()];
        for (i = 0; i < board.length(); i++)
            this.boardCollisionMap[i] = board.charAt(i) >= SOLID;
        
        // Build set of pathfinding nodes
        nodes = new Node[board.length()];
        for (i = 0; i < nodes.length; i++) {
            nodes[i] = new Node();
        }
    }
    
    /**
     * Finds a valid path from board positions startPos to destPos, within travelLimit steps
     * of travel. Returns the length of the path between tiles if a valid path is found, or
     * returns -1 if no valid path is found.
     * 
     * @param startPos
     * @param destPos
     * @param travelLimit
     * @param units
     * @return -1 if no path, or path length if valid path found
     */
    public int findPath(int startPos, int destPos, int travelLimit, ArrayList<GameUnit> units) {
        int i;
        // Build a collision map from the board collision
        boolean[] map = boardCollisionMap.clone();
        
        // add units as solid in collision map
        for (GameUnit unit : units) {
            // If it's not the unit at the start position (the unit we're moving) and it's not dead
            if (unit.getBoardPos() != startPos && unit.getHp() > 0)
                map[unit.getBoardPos()] = true; // Add to the collision map
        }
        
        // Initiate set of pathfinding nodes
        for (i = 0; i < map.length; i++) {
            // If solid tile
            if (map[i])
                nodes[i].init(true, 9999);
            else
                nodes[i].init(false, calculateHScore(i, destPos));
            
            // Find column and row of current node
            int c = i % boardWidth;
            int r = i / boardWidth;
            
            // If neighbours aren't solid, add them to neighbours[]
            // Neighbour to the left
            if (c > 0 && map[i-1] == false)
                nodes[i].neighbours[0] = i-1;
            // Neighbour to the right
            if (c < boardWidth - 1 && map[i+1] == false)
                nodes[i].neighbours[1] = i+1;
            // Neighbour to the top
            if (r > 0 && map[i-boardWidth] == false)
                nodes[i].neighbours[2] = i-boardWidth;
            // Neighbour to the bottom
            if (r < boardHeight - 1 && map[i+boardWidth] == false)
                nodes[i].neighbours[3] = i+boardWidth;
        }
        
        // Initiate open set
        int[] openSet = new int[map.length];
        openSet[0] = startPos;
        int openSetLength = 1;
        nodes[startPos].open = true;
        nodes[startPos].gScore = 0;
        nodes[startPos].fScore = nodes[startPos].hScore;
        
        // While open set isn't empty
        while (openSetLength > 0) {
            // Find node with lowest f score
            int lowestF = 9999;
            int currenti = -1;
            int current = 0;
            
            for (i = 0; i < openSetLength; i++) {
                if (nodes[openSet[i]].fScore < lowestF) {
                    lowestF = nodes[openSet[i]].fScore;
                    current = openSet[i];
                    currenti = i;
                }
            }
            
            // If it's the goal, finished
            if (current == destPos) {
                // calculate length of path
                int pathLength = 0;
                while (current != startPos) {
                    current = nodes[current].cameFrom;
                    pathLength++;
                }
                // Return path length
                return pathLength;
            }
            
            // Remove current from open set
            openSet[currenti] = openSet[openSetLength - 1];
            openSetLength--;
            nodes[current].closed = true;
            nodes[current].open = false;
            
            // For each neighbour
            int[] neighbours = nodes[current].neighbours;
            for (i = 0; i < 4; i++) {
                if (neighbours[i] == -1) continue;
                if (nodes[neighbours[i]].closed) continue;
                
                int tentativeGScore = nodes[current].gScore + 1;
                
                // Modification to the standard A* - if we're over the travel limit, ignore this neighbour
                if (tentativeGScore > travelLimit)
                    continue;
                
                if (nodes[neighbours[i]].open == false || tentativeGScore < nodes[neighbours[i]].gScore) {
                    nodes[neighbours[i]].cameFrom = current;
                    nodes[neighbours[i]].gScore = tentativeGScore;
                    nodes[neighbours[i]].fScore = tentativeGScore + nodes[neighbours[i]].hScore;
                    
                    if (nodes[neighbours[i]].open == false) {
                        openSet[openSetLength] = neighbours[i];
                        openSetLength++;
                        nodes[neighbours[i]].open = true;
                    }
                }
            }
        }
        
        // Return -1, indicates no path found
        return -1;
    }
    
    /**
     * Calculates the heuristic distance using the manhatten method
     * 
     * @param startPos
     * @param destPos
     * @return h(startPos,destPos)
     */
    private int calculateHScore(int startPos, int destPos) {
        return (int)(Math.abs((int)(startPos / boardWidth) - (int)(destPos / boardWidth))
                + Math.abs((startPos % boardWidth) - (destPos % boardWidth)));
    }
    
    /**
     * Describes a node within the pathfinding algorithm.
     */
    private class Node {
        public boolean solid;
        public boolean open;
        public boolean closed;
        public int cameFrom;
        public int hScore;
        public int gScore;
        public int fScore;
        public int[] neighbours = new int[4];
        
        public void init(boolean solid, int hScore) {
            this.solid = solid;
            this.open = false;
            this.closed = false;
            this.cameFrom = -1;
            this.hScore = hScore;
            this.gScore = -1;
            this.fScore = -1;
            this.neighbours[0] = -1;
            this.neighbours[1] = -1;
            this.neighbours[2] = -1;
            this.neighbours[3] = -1;
        }
    }
    
}
