/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.com3014.socketgame.model.client;

/**
 * Classes implementing iClientListener can listen for events from clients, such
 * as messages being received, or a client disconnecting.
 *
 * @author Ryan Pridgeon
 */
public interface iClientListener {
    
    /**
     * Called when a message is received by a client.
     * 
     * @param client
     * @param message 
     */
    void messageReceived(iClient client, Message message);
    
    /**
     * Called when a client disconnects.
     * 
     * @param client 
     */
    void clientDisconnect(iClient client);
}
