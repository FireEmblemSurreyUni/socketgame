/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.com3014.socketgame.model.client;

import java.util.ArrayList;

/**
 * The Message class holds data about a message received or being sent to a
 * client. The contents on the message depend on the type, which is an integer
 * referencing the types of messages defined in the protocol design document
 * (protocol_design_lobby_initial)
 *
 * @author Ryan Pridgeon
 */
public class Message {

    private int type = 0;
    private String name = null;
    private int uID = -1;
    private String status = null;
    private int opponentID = -1;
    private String message = null;
    private int teamNumber = -1;
    private int boardWidth = -1;
    private String board = null;
    private int actionID = -1;
    private int unitID = -1;
    private int boardPos = -1;
    
    private ArrayList<Message.User> userlist = new ArrayList<>();
    private ArrayList<Message.Unit> units = new ArrayList<>();
    private ArrayList<Message.HurtUnit> hurtUnits = new ArrayList<>();

    public Message(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUID() {
        return uID;
    }

    public void setUID(int uID) {
        this.uID = uID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public int getOpponentID() {
        return opponentID;
    }

    public void setOpponentID(int opponentID) {
        this.opponentID = opponentID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTeamNumber() {
        return teamNumber;
    }

    public void setTeamNumber(int teamNumber) {
        this.teamNumber = teamNumber;
    }

    public int getBoardWidth() {
        return boardWidth;
    }

    public void setBoardWidth(int boardWidth) {
        this.boardWidth = boardWidth;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public int getActionID() {
        return actionID;
    }

    public void setActionID(int actionID) {
        this.actionID = actionID;
    }

    public int getUnitID() {
        return unitID;
    }

    public void setUnitID(int unitID) {
        this.unitID = unitID;
    }

    public int getBoardPos() {
        return boardPos;
    }

    public void setBoardPos(int boardPos) {
        this.boardPos = boardPos;
    }


    public ArrayList<User> getUserlist() {
        return userlist;
    }
    
    public void addUser(int uID, String name, String status) {
        userlist.add(new User(uID, name, status));
    }
    
    public ArrayList<Unit> getUnits() {
        return units;
    }
    
    public void addUnit(int unitID, int teamNumber, String unitType, int boardPos) {
        units.add(new Unit(unitID, teamNumber, unitType, boardPos));
    }
    
    public ArrayList<HurtUnit> getHurtUnits() {
        return hurtUnits;
    }
    
    public void addHurtUnit(int unitID, int hp) {
        hurtUnits.add(new HurtUnit(unitID, hp));
    }

    @Override
    public String toString() {
        return "Message{" + "type=" + type + ", name=" + name + ", uID=" + uID + ", status="
                + status + ", opponentID=" + opponentID + ", message="
                + message + ", teamNumber=" + teamNumber + ", boardWidth=" + boardWidth + ", board="
                + board + ", userlist=" + userlist + ", units=" + units + '}';
    }
    
    public class User {
        
        private final int uID;
        private final String name;
        private final String status;

        public User(int uID, String name, String status) {
            this.uID = uID;
            this.name = name;
            this.status = status;
        }

        public int getUID() {
            return uID;
        }

        public String getName() {
            return name;
        }

        public String getStatus() {
            return status;
        }

        @Override
        public String toString() {
            return "User{" + "uID=" + uID + ", name=" + name + ", status=" + status + '}';
        }   
    }
    
    public class Unit {
        
        private final int unitID;
        private final int teamNumber;
        private final String unitType;
        private final int boardPos;

        public Unit(int unitID, int teamNumber, String unitType, int boardPos) {
            this.unitID = unitID;
            this.teamNumber = teamNumber;
            this.unitType = unitType;
            this.boardPos = boardPos;
        }

        public int getUnitID() {
            return unitID;
        }

        public int getTeamNumber() {
            return teamNumber;
        }

        public String getUnitType() {
            return unitType;
        }

        public int getBoardPos() {
            return boardPos;
        }

        @Override
        public String toString() {
            return "Unit{" + "unitID=" + unitID + ", teamNumber=" + teamNumber + ", unitType=" + unitType + ", boardPos=" + boardPos + '}';
        }
        
    }
    
    /**
     * Gives information about a unit that was hurt in a game message
     */
    public class HurtUnit {
        private int unitID;
        private int hp;

        public HurtUnit(int unitID, int hp) {
            this.unitID = unitID;
            this.hp = hp;
        }

        public int getUnitID() {
            return unitID;
        }

        public int getHp() {
            return hp;
        }
    }
}
