/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.com3014.socketgame.model.client;

/**
 * The iClient interface provides an interface for a single connected client, to be
 * used by the ClientMgr. It's implemented by classes which implement its functionality
 * using a method of communication. At the time of writing, this will be the WebsocketClient
 * class which implements the interface using raw websockets.
 *
 * @author Ryan Pridgeon
 */
public interface iClient {
    
    /**
     * Close the connection to this client
     */
    void close();
    
    /**
     * Set the listener which will listen for events such as messages from this client
     * 
     * @param listener 
     */
    void setClientListener(iClientListener listener);
    
    /**
     * Send the given Message to the client.
     * 
     * @param message 
     */
    void sendMessage(Message message);
    
    /**
     * Set the user's name
     * 
     * @param name 
     */
    void setName(String name);
    
    /**
     * Set the user ID
     * 
     * @param uID 
     */
    void setUID(int uID);
    
    /**
     * Get this client's username
     * 
     * @return name
     */
    String getName();
    
    /**
     * Get the user ID
     * 
     * @return uid
     */
    int getUID();
    
    /**
     * Get the last time in system time that ping was received from this client
     * 
     * @return system time ms
     */
    long getLastPingTime();
    
    /**
     * Returns false if the socket has been disconnected
     * 
     * @return boolean
     */
    boolean isAlive();
    
}
