/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.com3014.socketgame.model.client;

import com.com3014.socketgame.model.MessageTypes;
import com.com3014.socketgame.model.lobby.Lobby;
import com.com3014.socketgame.model.lobby.iLobbyListener;
import com.com3014.socketgame.service.GameService;
import java.util.ArrayList;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

/**
 * The ClientMgr class manages all connected clients in a list of iClients.
 * It implements iClientListener to listen for events from clients.
 *
 * @author Ryan Pridgeon
 */
public class ClientMgr implements iClientListener, iLobbyListener {
    
    /** How long in miliseconds between pings for a user to time out */
    private static final long TIME_OUT = 5000;

    /** A counter used to generate unique user ID's */
    private int uIDCounter = 0;
    
    /** A list of all connected clients */
    private ArrayList<iClient> clients = new ArrayList<>();
    
    /** The Lobby of users that aren't in a match */
    private Lobby lobby;
    
    public ClientMgr() {
        lobby = new Lobby(this);
    }
    
    /**
     * Add a client to the ClientMgr
     * 
     * @param client 
     * @param name - the username, if "null", guest is assumed
     */
    public void addClient(iClient client, String name) {
        clients.add(client);
        client.setClientListener(this);
        System.out.println("NEW CLIENT : ClientMgr : " + clients.size() + " clients connected");
        
        // set client's name and id
        client.setUID(uIDCounter);
        uIDCounter++;
        
        // set name
        if (name != null)
            client.setName(name);
        else
            client.setName("Guest" + client.getUID());
    }
    
    /**
     * Called when the given client finishes a game and needs to rejoin the lobby.
     * 
     * @param client 
     */
    public void gameEnd(iClient client) {
        //System.out.println("ClientMgr.gameEnd");
        
        lobby.addUser(client.getUID());
        
        sendUserList(client);
        
        changeStatus(client, "lobby");
        
    }
    
    /**
     * Finds a client in the clients list with the given UID.
     * 
     * @param uID
     * @return iClient
     */
    public iClient getClientByUID(int uID) {
        for (int i = 0; i < clients.size(); i++)
            if (clients.get(i).getUID() == uID)
                return clients.get(i);
        
        return null;
    }
    
    /**
     * Sends the list of connected users to the client, for when they join the lobby.
     * 
     * @param client 
     */
    private void sendUserList(iClient client) {
        // Create a type 5 message to give the new client the list of currently connected users
        Message userlist = new Message(MessageTypes.USER_LIST);

        // create userlist for message
        for (iClient clienti : clients)
            if (clienti != client)
                userlist.addUser(clienti.getUID(), clienti.getName(), "lobby");

        client.sendMessage(userlist);
    }
    
    /**
     * Called when a message is received by a client.
     * 
     * From iClientListener
     * 
     * @param client
     * @param message 
     */
    @Override
    public void messageReceived(iClient client, Message message) {
        
        switch (message.getType()) {
        case MessageTypes.CLIENT_CONNECTION: // new client connects with name
            
            // add client to lobby
            lobby.addUser(client.getUID());
            
            // Create type 1 message to give client their uid and name
            Message respond = new Message(MessageTypes.CLIENT_CONNECTION_RESPONSE);
            respond.setName(client.getName());
            respond.setUID(client.getUID());
            
            client.sendMessage(respond);
            
            sendUserList(client);
            
            // Create a type 2 message to inform connected clients of new user
            Message updateClients = new Message(MessageTypes.USER_CONNECTION_UPDATE);
            updateClients.setUID(client.getUID());
            updateClients.setName(client.getName());
            updateClients.setStatus("lobby");
            
            // send to other connected clients
            for (iClient clienti : clients)
                if (clienti != client)
                    clienti.sendMessage(updateClients);
            
            break;
            
        case MessageTypes.CLIENT_CHAT_MESSAGE: // client sends chat message
            
            System.out.println(client.getName() + ": " + message.getMessage());
            
            // Make a chat message to relay it to users
            Message chat = new Message(MessageTypes.SERVER_CHAT_MESSAGE);
            chat.setMessage(message.getMessage());
            chat.setUID(client.getUID());
            
            // send to everyone
            for (iClient clienti : clients)
                clienti.sendMessage(chat);
            
            break;
        }
        
        if (message.getType() >= 10 && message.getType() < 20)
            lobby.messageReceived(message.getType(), client.getUID(), message.getOpponentID());
    }
    
    /**
     * Called when a client disconnects.
     * 
     * From iClientListener
     * 
     * @param client 
     */
    @Override
    public void clientDisconnect(iClient client) {
        
        System.out.println("ClientMgr.clientDisconnect " + client.getName());
        
        clients.remove(client);
        
        //System.out.println("clients contains " + client.getName() + " = " + clients.contains(client));
        
        // Remove from lobby
        lobby.removeUser(client.getUID());
        
        // create message to inform other clients of user disconnect
        Message message = new Message(MessageTypes.USER_DISCONNECT_UPDATE);
        message.setUID(client.getUID());
        
        // send to clients
        for (iClient clienti : clients)
            clienti.sendMessage(message);
    }
    
    /**
     * From iLobbyListener
     * 
     * Send a message to the user given by destinationUID, containing the given
     * message type and opponentID.
     * 
     * @param destinationUID - The user to send the message to
     * @param type - the message type
     * @param opponentID - the message opponentID
     */
    @Override
    public void sendLobbyMessage(int destinationUID, int type, int opponentID) {
        
        // Create a message
        Message message = new Message(type);
        message.setOpponentID(opponentID);
        
        // Send to client
        for (iClient clienti : clients)
            if (clienti.getUID() == destinationUID)
                clienti.sendMessage(message);
    }
    
    /**
     * From iLobbyListener
     * 
     * Tells the listener to start a game between the given lobby users.
     * Starts a new game using GameService with the given users.
     * 
     * @param uID1 - User ID of the first user
     * @param uID2 - User ID of the second user
     */
    @Override
    public void startGame(int uID1, int uID2) {
        System.out.println("GAME STARTING : ClientMgr : " + uID1 + " and " + uID2);
        
        // Get the web application context
        WebApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        
        try {
            // Get a new GameService bean to manage the game
            GameService game = (GameService)ctx.getBean(GameService.class);
            
            iClient client1 = getClientByUID(uID1);
            iClient client2 = getClientByUID(uID2);
            
            // remove them from lobby
            lobby.removeUser(uID1);
            lobby.removeUser(uID2);
            
            // change their status to ingame
            changeStatus(client1, "ingame");
            changeStatus(client2, "ingame");
            
            // Initiate game with the playing clients
            game.init(client1, client2, this);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Change the status of a client and update other connected users.
     * 
     * @param client
     * @param status 
     */
    private void changeStatus(iClient client, String status) {
        
        Message statusUpdate = new Message(MessageTypes.USER_STATUS_CHANGE_UPDATE);
        statusUpdate.setStatus(status);
        statusUpdate.setUID(client.getUID());
        
        for (iClient clienti : clients) {
            clienti.sendMessage(statusUpdate);
        }
    }
    
    public void ping() {
        //System.out.println("Ping");
        
        // Create a ping message
        Message pingMsg = new Message(MessageTypes.PING_PONG);
        
        // Work out the minimum lastPingTime for a client to not time out
        long minPingTime = System.currentTimeMillis() - (long)TIME_OUT;
        
        // List of clients that timed out
        ArrayList<iClient> clientsToDisconnect = new ArrayList<>();
        
        // For each client in the manager
        for (iClient clienti : clients) {
            // If they haven't pinged since minPingTime
            if (clienti.getLastPingTime() < minPingTime)
                clientsToDisconnect.add(clienti); // Add them to the list of users to disconnect
            else if (clienti.isAlive())
                clienti.sendMessage(pingMsg); // Otherwise send a ping
        }
        
        // For each client to be disconnected
        for (iClient clienti : clientsToDisconnect) {
            System.out.println("Client Time out " + clienti.getName());
            clienti.close(); // close the connection
            if (clients.contains(clienti))
                clients.remove(clienti);
        }
    }
}
