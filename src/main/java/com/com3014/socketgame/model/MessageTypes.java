/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.com3014.socketgame.model;

/**
 * The different message types as specified in the protocol design document.
 * Declared as final ints instead of enum, so can be compared to integers easily.
 *
 * @author Ryan Pridgeon
 */
public class MessageTypes {
    
    public static final int CLIENT_CONNECTION = 0;
    public static final int CLIENT_CONNECTION_RESPONSE = 1;
    public static final int USER_CONNECTION_UPDATE = 2;
    public static final int USER_DISCONNECT_UPDATE = 3;
    public static final int USER_STATUS_CHANGE_UPDATE = 4;
    public static final int USER_LIST = 5;
    public static final int CLIENT_CHAT_MESSAGE = 6;
    public static final int SERVER_CHAT_MESSAGE = 7;
    public static final int PING_PONG = 8;
    public static final int CLIENT_CHALLENGE = 10;
    public static final int CHALLENGE_SEND = 11;
    public static final int CHALLENGE_ACCEPT = 12;
    public static final int CHALLENGE_REJECT = 13;
    public static final int CHALLENGE_REJECT_UPDATE = 14;
    public static final int CHALLENGE_CANCEL = 15;
    public static final int CHALLENGE_CANCEL_UPDATE = 16;
    public static final int START_GAME = 20;
    public static final int GAME_INFO = 21;
    public static final int CLIENT_GAME_READY = 22;
    public static final int SERVER_GAME_READY = 23;
    public static final int GAME_CANCELLED = 24;
    public static final int GAME_ENDED = 25;
    public static final int CLIENT_GAME_QUIT = 26;
    public static final int GAME_INVALID_ACTION = 40;
    public static final int GAME_CLIENT_MOVE = 41;
    public static final int GAME_SERVER_MOVE = 42;
    public static final int GAME_CLIENT_ATTACK = 43;
    public static final int GAME_SERVER_ATTACK = 44;
    public static final int GAME_CLIENT_END_TURN = 45;
    public static final int GAME_SERVER_END_TURN = 46;
}
