/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.com3014.socketgame.model.lobby;

/**
 * A challenge between 2 players, sent by the instigator who
 * is challenging the receiver. These are managed by Lobby.
 * It implements Comparable so that it can be ordered with
 * other MatchChallenges by its instigator ID.
 *
 * @author Ryan Pridgeon
 */
public class MatchChallenge implements Comparable<MatchChallenge> {

    /**
     * CompareTo function to order MatchChallenges in a set or list by their instigatorID field
     * in ascending order (for binary search)
     * 
     * @param o
     * @return 
     */
    @Override
    public int compareTo(MatchChallenge o) {
        if (this.getInstigatorID() > o.getInstigatorID())
            return 1;
        else if (this.getInstigatorID() < o.getInstigatorID())
            return -1;
        else if (this.getReceiverID() > o.getReceiverID())
            return 1;
        else if (this.getReceiverID() < o.getReceiverID())
            return -1;
        else
            return 0;
    }
    
    /** The UID of the user who sent the challenge */
    private int instigatorID;
    
    /** The UID of the user who was challenged */
    private int receiverID;
    
    /**
     * Construct a MatchChallenge between the given users
     * 
     * @param instigatorID - UID of the sender
     * @param receiverID - UID of the receiver
     */
    public MatchChallenge(int instigatorID, int receiverID) {
        this.instigatorID = instigatorID;
        this.receiverID = receiverID;
    }

    public int getInstigatorID() {
        return instigatorID;
    }

    public int getReceiverID() {
        return receiverID;
    }
}
