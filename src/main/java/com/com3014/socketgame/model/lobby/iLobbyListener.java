/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.com3014.socketgame.model.lobby;

/**
 * Classes that want to listen to a Lobby should implement this.
 * Gets added to a Lobby on initialisation.
 * 
 * @author Ryan Pridgeon
 */
public interface iLobbyListener {
    
    /**
     * Send a message to the user given by destinationUID, containing the given
     * message type and opponentID.
     * 
     * @param destinationUID - The user to send the message to
     * @param type - the message type
     * @param opponentID - the message opponentID
     */
    void sendLobbyMessage(int destinationUID, int type, int opponentID);
    
    /**
     * Tells the listener to start a game between the given lobby users.
     * 
     * @param uID1 - User ID of the first user
     * @param uID2 - User ID of the second user
     */
    void startGame(int uID1, int uID2);
    
}
