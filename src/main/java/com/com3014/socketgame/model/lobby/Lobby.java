/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.com3014.socketgame.model.lobby;

import com.com3014.socketgame.model.MessageTypes;
import java.util.ArrayList;

/**
 * The Lobby class manages the lobby of users not in a game. It handles challenges
 * sent between players. It needs to be updated of changes to user status,
 * and any lobby-relevant messages received from clients. It sends events to its
 * listener (iLobbyListener)
 * 
 * @author Ryan Pridgeon
 */
public class Lobby {
    
    /** The list of LobbyUser's connected to the lobby */
    private ArrayList<LobbyUser> users = new ArrayList<>();
    
    /** The list of challenges */
    private ArrayList<MatchChallenge> challenges = new ArrayList<>();
    
    /** The listener */
    private iLobbyListener listener;
    
    /**
     * Construct a new instance of lobby with the given listener
     * 
     * @param listener 
     */
    public Lobby(iLobbyListener listener) {
        this.listener = listener;
    }
    
    /**
     * Adds a new user to the lobby
     * @param uID 
     */
    public void addUser(int uID) {
        users.add(new LobbyUser(uID));
        
        //System.out.println("User added to lobby, users: " + users.size());
    }
    
    /**
     * Removes a user from the lobby
     * @param uID 
     */
    public void removeUser(int uID) {
        int i;
        MatchChallenge ch;
        // Check if user is involved in any challenges
        for (i = challenges.size() - 1; i >= 0; i--) {
            ch = challenges.get(i);
            if (ch.getInstigatorID() == uID) {
                // If user instigated this challenge
                // Send cancel to challengee
                listener.sendLobbyMessage(ch.getReceiverID(), MessageTypes.CHALLENGE_CANCEL_UPDATE, uID);
                challenges.remove(ch);
            } else if (ch.getReceiverID() == uID) {
                // If user was challenged
                // Send reject to challengee
                listener.sendLobbyMessage(ch.getInstigatorID(), MessageTypes.CHALLENGE_REJECT_UPDATE, uID);
                challenges.remove(ch);
            }
        }
        
        for (i = users.size() - 1; i >= 0; i--) {
            if (users.get(i).getUID() == uID)
                users.remove(i);
        }
        
        //System.out.println("User removed from lobby, users: " + users.size());
    }
    
    /**
     * A message received from the given client that's relevant to the lobby.
     * For example a player challenging another player.
     * 
     * @param type - message type
     * @param senderID - uID of the sender
     * @param opponentID - uID in the message
     */
    public void messageReceived(int type, int senderID, int opponentID) {
        
        switch (type) {
            // Client challenges another client
            case MessageTypes.CLIENT_CHALLENGE:
                addChallenge(senderID, opponentID);
                
                // send challenge to challenged client
                listener.sendLobbyMessage(opponentID, MessageTypes.CHALLENGE_SEND, senderID);
                break;
                
            // Client cancels a challenge they made
            case MessageTypes.CHALLENGE_CANCEL:
                removeChallenge(senderID, opponentID);
                
                // Inform clients that challenge was cancelled
                listener.sendLobbyMessage(opponentID, MessageTypes.CHALLENGE_CANCEL_UPDATE, senderID);
                break;
                
            // Challengee rejects challenge
            case MessageTypes.CHALLENGE_REJECT:
                removeChallenge(opponentID, senderID);
                
                // Inform challenger of rejection
                listener.sendLobbyMessage(opponentID, MessageTypes.CHALLENGE_REJECT_UPDATE, senderID);
                
            // Challengee accepts challenge
            case MessageTypes.CHALLENGE_ACCEPT:
                removeChallenge(opponentID, senderID);
                
                // Start game between challenger and challengee
                listener.startGame(senderID, opponentID);
        }
    }
    
    private void addChallenge(int instigatorID, int receiverID) {
        int i;
        MatchChallenge challenge = new MatchChallenge(instigatorID, receiverID);
        for (i = 0; challenge.compareTo(challenge) < 0; i++)
            /* */;
        challenges.add(i, challenge);
    }
    
    private void removeChallenge(int instigatorID, int receiverID) {
        challenges.remove(getChallenge(instigatorID, receiverID));
    }
    
    private MatchChallenge getChallenge(int instigatorID, int receiverID) {
        MatchChallenge challenge;
        for (int i = 0; i < challenges.size(); i++) {
            challenge = challenges.get(i);
            if (challenge.getInstigatorID() == instigatorID
                    && challenge.getReceiverID() == receiverID)
                return challenge;
        }
        return null; // null if not found
    }
}
