/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.com3014.socketgame.model.lobby;

/**
 * Holds data for a user connected to the lobby.
 * 
 * @author Ryan Pridgeon
 */
public class LobbyUser {
    
    private int uID = -1;
    
    LobbyUser(int uID) {
        this.uID = uID;
    }
    
    public int getUID() {
        return this.uID;
    }
}
