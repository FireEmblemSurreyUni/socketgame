package com.com3014.socketgame.model;

/**
 *
 * @author stephenlarkin
 */
public class GameStatistics {
  
  private int totalGamesPlayed;
  private int totalGamesWon;
  
  public GameStatistics(int total, int won) {
    this.totalGamesPlayed = total;
    this.totalGamesWon = won;
  }

  public int getTotalGamesPlayed() {
    return this.totalGamesPlayed;
  }

  public int getTotalGamesWon() {
    return this.totalGamesWon;
  }
  
  public int getTotalGamesLost() {
    return this.totalGamesPlayed - this.totalGamesWon;
  }
}
