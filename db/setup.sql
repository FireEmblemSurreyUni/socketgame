-- Drops and revokes, kept for debugging purposes

DROP TABLE game.user_stats;

DROP TABLE game.user_roles;

DROP TABLE game.users;

REVOKE ALL PRIVILEGES ON SCHEMA game FROM gameroot;

REVOKE ALL PRIVILEGES ON DATABASE gamedb FROM gameroot;

DROP USER gameroot;

DROP SCHEMA game;

-- Create user for aceess to the game database
CREATE USER gameroot WITH PASSWORD 'gameroot';

GRANT CONNECT ON DATABASE gamedb TO gameroot;

-- Create a schema for the game tables
CREATE SCHEMA game;

GRANT USAGE ON SCHEMA game TO gameroot;

-- User Table
CREATE TABLE game.users (
  id            SERIAL                      PRIMARY KEY
, email         VARCHAR(100)                NOT NULL
, username      VARCHAR(30)                 NOT NULL
, pass_hash     VARCHAR(64)                 NOT NULL
, date_created  TIMESTAMP WITHOUT TIME ZONE NOT NULL
, date_updated  TIMESTAMP WITHOUT TIME ZONE
, date_accessed TIMESTAMP WITHOUT TIME ZONE
, enabled       SMALLINT                    NOT NULL DEFAULT 1
);

CREATE UNIQUE INDEX users_email_lower_idx ON game.users (lower(email));

CREATE INDEX users_pass_hash_idx ON game.users (pass_hash);

-- Add a constraint that all of the users have different emails
ALTER TABLE game.users
ADD CONSTRAINT users_chk_email UNIQUE (email);

GRANT INSERT, DELETE, UPDATE, SELECT ON game.users TO gameroot;

GRANT USAGE, SELECT ON SEQUENCE game.users_id_seq TO gameroot;

-- User roles table
CREATE TABLE game.user_roles (
  ur_id SERIAL      PRIMARY KEY
, u_id  INT         NOT NULL
, role  VARCHAR(45) NOT NULL
);

ALTER TABLE game.user_roles
ADD FOREIGN KEY (u_id)
REFERENCES game.users (id);

CREATE INDEX user_roles_idx ON game.user_roles (u_id,role);

CREATE INDEX user_roles_uid_idx ON game.user_roles (u_id);

GRANT INSERT, DELETE, UPDATE, SELECT ON game.user_roles TO gameroot;

GRANT USAGE, SELECT ON SEQUENCE game.user_roles_ur_id_seq TO gameroot;

-- User statistics table
CREATE TABLE game.user_stats (
  u_id         INT           PRIMARY KEY
, games_won    INT DEFAULT 0 NOT NULL
, games_played INT DEFAULT 0 NOT NULL
);

ALTER TABLE game.user_stats
ADD FOREIGN KEY (u_id)
REFERENCES game.users (id);

GRANT INSERT, DELETE, UPDATE, SELECT ON game.user_stats TO gameroot;

INSERT INTO game.users (
  email
, username
, pass_hash
, date_created
) VALUES (
  'a@test.com'
, 'admin'
, 'admin'
, CURRENT_TIMESTAMP
);

INSERT INTO game.user_roles (
  u_id
, role
) VALUES (
  1
, 'ROLE_USER'
);

INSERT INTO game.user_roles (
  u_id
, role
) VALUES (
  1
, 'ROLE_ADMIN'
);

INSERT INTO game.user_stats (
  u_id
, games_won
, games_played
) VALUES (
  1
, 0
, 0
);
